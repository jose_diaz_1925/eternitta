<?php
/**
 * eternitta functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eternitta
 */

if ( ! function_exists( 'eternitta_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eternitta_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on eternitta, use a find and replace
	 * to change 'eternitta' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'eternitta', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'eternitta' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'eternitta_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // eternitta_setup
add_action( 'after_setup_theme', 'eternitta_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eternitta_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'eternitta_content_width', 640 );
}
add_action( 'after_setup_theme', 'eternitta_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eternitta_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'eternitta' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar-transposh', 'eternitta' ),
        'id'            => 'sidebar-2',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'eternitta_widgets_init' );

if ( !function_exists( 'optionsframework_init' ) ) {
    define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
    require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}


/**
 * Enqueue scripts and styles.
 */
function eternitta_scripts() {
	wp_enqueue_style( 'eternitta-style', get_stylesheet_uri() );
    wp_enqueue_style( 'fonts', get_template_directory_uri().'/css/fonts.css' );
    wp_enqueue_style( 'fuentes_pulsera', get_template_directory_uri().'/css/fuentes_pulsera.css' );
    wp_enqueue_style( 'foundation', get_template_directory_uri().'/css/foundation.min.css' );
    wp_enqueue_style( 'icons', get_template_directory_uri().'/icons/flaticon.css' );
    wp_enqueue_style( 'loader', get_template_directory_uri().'/css/loader.css' );
    wp_enqueue_style( 'owl-carrusel', get_template_directory_uri().'/css/owl.carousel.css' );
    wp_enqueue_style( 'owl-carrusel-default', get_template_directory_uri().'/css/owl.theme.default.css' );
    wp_enqueue_style( 'style_general', get_template_directory_uri().'/css/style_general.css' );

    wp_enqueue_script( 'paises', get_template_directory_uri() . '/js/paises.js', array(), '20120206', true );
    wp_enqueue_script( 'htmlcanvas', get_template_directory_uri() . '/js/html2canvas.js', array(), '20120206', true );
    wp_enqueue_script( 'lodash', get_template_directory_uri() . '/js/lodash.js', array(), '20120206', false );
    wp_enqueue_script( 'owl-carrusel-function', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20120206', true );
	wp_enqueue_script( 'eternitta-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
    wp_enqueue_script( 'main-admin', get_template_directory_uri() . '/js/main-admin.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'eternitta-paises', get_template_directory_uri() . '/js/paises.js', array(), '20120206', true );
    wp_enqueue_script( 'eternitta-lodash', get_template_directory_uri() . '/js/lodash.js', array(), '20120206', true );
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '20120206', true );
	wp_enqueue_script( 'eternitta-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eternitta_scripts' );


function wp_eternita_admin_style() {
    wp_enqueue_script( 'main-admin', get_template_directory_uri() . '/js/main-admin.js', array('jquery'), '20120206', true );
    wp_enqueue_style( 'admincustom-style', get_template_directory_uri() . '/css/admincustom-style.css' );
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
}
add_action('admin_print_scripts', 'wp_eternita_admin_style');

add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type('Productos',
        array(
            'labels' => array(
                'name' => __('productos'),
                'singular_name' => __('producto')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title', 'thumbnail')
        )
    );

    register_post_type('Distribuidores',
        array(
            'labels' => array(
                'name' => __('distribuidores'),
                'singular_name' => __('distribuidor')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title')
        )
    );

    register_post_type('Pedidos',
        array(
            'labels' => array(
                'name' => __('pedidos'),
                'singular_name' => __('pedidos')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title')
        )
    );
}

add_action( 'init', 'crea_taxonimies' );
function crea_taxonimies(){
    register_taxonomy(
        'tallas',
        'productos',
        array(
            'label' => __('tallas'),
            'sort' => true,
            'args' => array('orderby' => 'term_order'),
            'rewrite' => array('slug' => 'botones'),
            'hierarchical'=>true,
        )
    );
}


add_action( 'tallas_edit_form_fields', 'tallas_taxonomy_custom_fields', 10, 2 );
add_action( 'tallas_add_form_fields', 'tallas_taxonomy_custom_fields', 10, 2 );
add_action( 'edited_tallas', 'save_taxonomy_custom_fields_tallas', 10, 2 );
add_action ('created_tallas', 'save_taxonomy_custom_fields_tallas',10, 2);

function tallas_taxonomy_custom_fields($tag) {
    $t_id = $tag->term_id;
    $term_meta = get_option( "tallas_$t_id" );
    ?>

    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Nombre de la talla'); ?></label>
        </th>
        <td>
            <input class="nombre-field" type="text" name="tallas[nombre]" value="<?php echo $term_meta['nombre'] ? $term_meta['nombre'] : ''; ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Nombre en ingles'); ?></label>
        </th>
        <td>
            <input class="nombre-field" type="text" name="tallas[nombreus]" value="<?php echo $term_meta['nombre'] ? $term_meta['nombreus'] : ''; ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Ancho'); ?></label>
        </th>
        <td>
            <input class="precio-field" type="text" name="tallas[ancho]" value="<?php echo $term_meta['ancho'] ? $term_meta['ancho'] : ''; ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Largo'); ?></label>
        </th>
        <td>
            <input class="precio-field" type="text" name="tallas[largo]" value="<?php echo $term_meta['largo'] ? $term_meta['largo'] : ''; ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Precio MX'); ?></label>
        </th>
        <td>
            <input class="precio-field" type="number" name="tallas[precio]" value="<?php echo $term_meta['precio'] ? $term_meta['precio'] : ''; ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Precio USD'); ?></label>
        </th>
        <td>
            <input class="precio-field" type="number" name="tallas[preciousd]" value="<?php echo $term_meta['preciousd'] ? $term_meta['preciousd'] : ''; ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="presenter_id"><?php _e('Precio Euro'); ?></label>
        </th>
        <td>
            <input class="precio-field" type="number" name="tallas[precioeuro]" value="<?php echo $term_meta['precioeuro'] ? $term_meta['precioeuro'] : ''; ?>" />
        </td>
    </tr>
<?php
}

function save_taxonomy_custom_fields_tallas( $term_id ) {
    if ( isset( $_POST['tallas'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "tallas_$t_id" );
        $cat_keys = array_keys( $_POST['tallas'] );
        foreach ( $cat_keys as $key ){
            if ( isset( $_POST['tallas'][$key] ) ){
                $term_meta[$key] = $_POST['tallas'][$key];
            }
        }
        //save the option array
        update_option( "tallas_$t_id", $term_meta );
    }
}

add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
    if ($template_file == 'my-templates/home.php') {
        add_meta_box('cyb-meta-beneficios', __('Beneficios'), 'cyb_meta_beneficios', 'page');
        add_meta_box('cyb-meta-tallas', __('Texto tallas'), 'cyb_meta_talllas', 'page');
    }
    if ($template_file == 'my-templates/tpl_nosotros.php') {
        add_meta_box( 'cyb-meta-nosotros', __('Quienes somos'), 'cyb_meta_nosotros','page' );
        add_meta_box( 'cyb-meta-mision', __('Mision'), 'cyb_meta_mision','page' );
        add_meta_box( 'cyb-meta-vision', __('Vision'), 'cyb_meta_vision','page' );
        add_meta_box( 'cyb-meta-valores', __('Valores'), 'cyb_meta_valores','page' );
    }
    add_meta_box( 'cyb-meta-metadata', __('Atributos'), 'cyb_meta_metadata','distribuidores','normal','high' );
    add_meta_box( 'cyb-meta-prodxpedido', __('Productos del pedido'), 'cyb_meta_prodxpedido','pedidos','normal','high' );
    add_meta_box( 'cyb_meta_compradorpedido', __('Datos del comprador'), 'cyb_meta_compradorpedido','pedidos' );
    add_meta_box( 'cyb_meta_infoenvio', __('Datos de envío'), 'cyb_meta_infoenvio','pedidos' );
    add_meta_box( 'cyb_meta_status', __('Estatus del envio'), 'cyb_meta_status','pedidos','side','default');
    add_meta_box( 'cyb_meta_distribuidor', __('Datos del distribuidor'), 'cyb_meta_distribuidor','pedidos','normal','default' );
    add_meta_box( 'cyb-meta-piedra', __('Piedras'), 'cyb_meta_piedra','productos' );
    add_meta_box( 'cyb-meta-galeria', __('Galeria de imagenes'), 'cyb_meta_galeria','productos' );
}


function cyb_meta_status(){
    global $post;
    $estatus = get_post_meta($post->ID,'estatus',true);

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div class="contenedor_infodistribuidor">';
    $html .= '<label>Estatus</label><br>';
    $html .= '<select name="status" >';
    switch ($estatus) {
        case 'Nuevo':
            $html .='<option selected="selected" value="Nuevo">Nuevo</option>';
            $html .='<option value="En proceso">En proceso</option>';
            $html .='<option value="Esperando">Esperando</option>';
            $html .='<option value="Enviado">Enviado</option>';
            $html .='<option value="Enviado y entregado">Enviado y entregado</option>';
            $html .='<option value="Cancelado">Cancelado</option>';
            break;
        case 'En proceso':

            $html .='<option value="Nuevo">Nuevo</option>';
            $html .='<option selected="selected" value="En proceso">En proceso</option>';
            $html .='<option value="Esperando">Esperando</option>';
            $html .='<option value="Enviado">Enviado</option>';
            $html .='<option value="Enviado y entregado">Enviado y entregado</option>';
            $html .='<option value="Cancelado">Cancelado</option>';

            break;
        case 'Esperando':

            $html .='<option value="Nuevo">Nuevo</option>';
            $html .='<option value="En proceso">En proceso</option>';
            $html .='<option selected="selected" value="Esperando">Esperando</option>';
            $html .='<option value="Enviado">Enviado</option>';
            $html .='<option value="Enviado y entregado">Enviado y entregado</option>';
            $html .='<option value="Cancelado">Cancelado</option>';

            break;
        case 'Enviado':

            $html .='<option value="Nuevo">Nuevo</option>';
            $html .='<option value="En proceso">En proceso</option>';
            $html .='<option value="Esperando">Esperando</option>';
            $html .='<option selected="selected" value="Enviado">Enviado</option>';
            $html .='<option value="Enviado y entregado">Enviado y entregado</option>';
            $html .='<option value="Cancelado">Cancelado</option>';

            break;
        case 'Enviado y entregado':

            $html .='<option value="Nuevo">Nuevo</option>';
            $html .='<option value="En proceso">En proceso</option>';
            $html .='<option value="Esperando">Esperando</option>';
            $html .='<option value="Enviado">Enviado</option>';
            $html .='<option selected="selected" value="Enviado y entregado">Enviado y entregado</option>';
            $html .='<option value="Cancelado">Cancelado</option>';

            break;
        case 'Cancelado':

            $html .='<option value="Nuevo">Nuevo</option>';
            $html .='<option value="En proceso">En proceso</option>';
            $html .='<option value="Esperando">Esperando</option>';
            $html .='<option value="Enviado">Enviado</option>';
            $html .='<option value="Enviado y entregado">Enviado y entregado</option>';
            $html .='<option selected="selected" value="Cancelado">Cancelado</option>';

            break;
    }
    $html .= '</select>';
    $html .= '</div>';

    echo $html;
}

function cyb_meta_galeria(){
    global $post;
    $songs = get_post_meta($post->ID,'galeria_imagenes',true);
    if(!empty($songs)){
        $data = json_decode($songs);
        $contador = 0;
        foreach($data as $img){
            if(!empty($img)) {
                echo "<div class='img_" . $contador . "' style='float: left; overflow: hidden; position: relative; width: 15%;display: inline; margin: 10px;'><img style='width: 100%;' src='" . $img->data->urlarchivo . "' ><p>Archivo: $img->namearchivo</p><a data-position='" . $contador . "' class='remove_img' name='" . $img->namearchivo . "' style='position: absolute;right: 0px;top: 0;background: rgba(0,0,0,0.7);padding: 5px;color: #fff;text-decoration: none;' >Borrar</a></div>";
            }
            $contador++;
        }
        echo '<div style="clear:both"></div>';
    }
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div style="clear: both;"></div>';
    $html .='<div class="img_gal">';
    $html .= '<div class="items">';
    $html .= '</div>';
    $html .= '<div style="clear: both;"></div>';
    $html .= "<input style='display:none;' id='image_location' type='text' name='galeria_imagenes' value='".$songs."' size='50'>";
    $html .= '<input data-contador="'.$contador.'" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar archivo" />';
    $html .= '</div>';
    echo $html;
}

function cyb_meta_metadata (){
    global $post;

    $nombre = get_post_meta($post->ID,'nombre',true);
    $apellido = get_post_meta($post->ID,'apellido',true);
    $contrasena = get_post_meta($post->ID,'contrasena',true);
    $country = get_post_meta($post->ID,'country',true);
    $state = get_post_meta($post->ID,'state',true);
    $ciudad = get_post_meta($post->ID,'ciudad',true);
    $direccion = get_post_meta($post->ID,'direccion',true);
    $codigo_postal = get_post_meta($post->ID,'codigo_postal',true);
    $email = get_post_meta($post->ID,'email',true);
    $telefono = get_post_meta($post->ID,'telefono',true);

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div class="contenedor_infodistribuidor">';
    $html .= '<label>Nombre</label><br>';
    $html .= '<input type="text" name="nombre" value="'.$nombre.'" ><br><br>';
    $html .= '<label>Apellido</label><br>';
    $html .= '<input type="text" name="apellido" value="'.$apellido.'" ><br><br>';
    $html .= '<label>contrasena</label><br>';
    $html .= '<input type="password" name="contrasena" value="'.$contrasena.'" ><br><br>';
    $html .= '<label>Pais</label><br>';
    $html .= '<input type="text" name="country" value="'.$country.'" ><br><br>';
    $html .= '<label>Estado</label><br>';
    $html .= '<input type="text" name="state" value="'.$state.'" ><br><br>';
    $html .= '<label>Cuidad</label><br>';
    $html .= '<input type="text" name="ciudad" value="'.$ciudad.'" ><br><br>';
    $html .= '<label>Dirección</label><br>';
    $html .= '<textarea name="direccion" >'.$direccion.'</textarea><br><br>';
    $html .= '<label>Codigo postal</label><br>';
    $html .= '<input type="text" name="codigo_postal" value="'.$codigo_postal.'" ><br><br>';
    $html .= '<label>Email</label><br>';
    $html .= '<input type="text" name="email" value="'.$email.'" ><br><br>';
    $html .= '<label>Teléfono</label><br>';
    $html .= '<input type="text" name="telefono" value="'.$telefono.'" ><br><br>';
    $html .= '</div>';

    echo $html;

}


function cyb_meta_infoenvio (){
    global $post;

    $contador=0;
    $direccion_envio=get_post_meta($post->ID,'direccion_envio',true);

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');


    echo '<div class="contenedor_prodcutos">';
    echo '<p>Nombre : '.utf8_decode($direccion_envio['nombre']).'</p>';
    echo '<p>Calle : '.$direccion_envio['calle'].'</p>';
    echo '<p>Numero exterior : '.$direccion_envio['numero_exterior'].'</p>';
    echo '<p>Numero interior : '.$direccion_envio['numero_interior'].'</p>';
    echo '<p>Cruzamientos : '.$direccion_envio['cruzamientos'].'</p>';
    echo '<p>Colonia : '.utf8_decode($direccion_envio['colonia']).'</p>';
    echo '<p>Pais : '.utf8_decode($direccion_envio['pais']).'</p>';
    echo '<p>Estado : '.utf8_decode($direccion_envio['estado']).'</p>';
    echo '<p>Ciudad : '.utf8_decode($direccion_envio['ciudad']).'</p>';
    echo '<p>C.P : '.$direccion_envio['cp'].'</p>';
    echo '<p>Teléfono : '.$direccion_envio['telefono'].'</p>';
    echo '<p>Celular : '.$direccion_envio['celular'].'</p>';
    echo '<p>Correo eléctronico : '.$direccion_envio['email'].'</p>';
    echo '</div>';
}

function cyb_meta_distribuidor (){
    global $post;

    $contador=0;
    $data=get_post_meta($post->ID,'datos_distribuidor',true);
    $datos_distribuidor=json_decode($data);

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');


    if($data!='') {
        echo '<div class="contenedor_prodcutos">';
        echo '<p>Nombre : ' . $datos_distribuidor->nombre . '</p>';
        echo '<p>País : ' . $datos_distribuidor->country . '</p>';
        echo '<p>Estado : ' . $datos_distribuidor->state . '</p>';
        echo '<p>Ciudad : ' . $datos_distribuidor->ciudad . '</p>';
        echo '<p>Dirección : ' . $datos_distribuidor->direccion . '</p>';
        echo '<p>Correo electrónico : ' . $datos_distribuidor->email . '</p>';
        echo '<p>Teléfono : ' . $datos_distribuidor->telefono . '</p>';
        echo '</div>';
    }
    else{
        echo '<p>Este pedido no fue hecho por un distribuidor</p>';
    }
}

function cyb_meta_compradorpedido (){
    global $post;

    $contador=0;
    //print_r($productosxpedido);
    $nombre_comprador=get_post_meta($post->ID,'payer_name',true);
    $email_comprador=get_post_meta($post->ID,'payer_email',true);
    $telefono_comprador=get_post_meta($post->ID,'payer_phone',true);

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');


    echo '<div class="contenedor_prodcutos">';
    echo '<p>Nombre del comprador : '.$nombre_comprador.'</p>';
    echo '<p>Email del comprador : '.$email_comprador.'</p>';
    echo '<p>Teléfono del comprador : '.$telefono_comprador.'</p>';
    echo '</div>';
}


function cyb_meta_prodxpedido (){
    global $post;

    $prod = get_post_meta($post->ID,'resumen_productos',true);
    $productosxpedido=json_decode($prod);
    $contador=1;

    //print_r($productosxpedido);

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    echo '<div class="contenedor_prodcutos">';
    foreach($productosxpedido as $item_prod){

        if($item_prod->disenopulsera->tallas->talla_s!='' && $item_prod->disenopulsera->tallas->talla_s > 0){
            $talla_elegida='S';
        }
        if($item_prod->disenopulsera->tallas->talla_m!='' && $item_prod->disenopulsera->tallas->talla_m > 0 ){
            $talla_elegida='M';
        }
        if($item_prod->disenopulsera->tallas->talla_l!='' && $item_prod->disenopulsera->tallas->talla_l > 0){
            $talla_elegida='L';
        }
        if($item_prod->disenopulsera->tallas->talla_p!='' && $item_prod->disenopulsera->tallas->talla_p > 0){
            $talla_elegida='P';
        }


        echo '<br>';
        echo '<ul class="columnas">';
        echo '<li class="img_pulsera"><img data-urlimg="'.get_template_directory_uri().'/pulseras_generadas/'.$item_prod->disenopulsera->imgpulsera.'" src="'.get_template_directory_uri().'/pulseras_generadas/'.$item_prod->disenopulsera->imgpulsera.'"></li>';
        echo '<li>Nombre: '.$item_prod->datos_generales->nombre.'</li>';
        echo '<li>Talla elegida: '.$talla_elegida.'</li>';
        echo '<li>Teléfono: '.$item_prod->datos_generales->telefono.'</li>';
        echo '<li>Fecha de nacimiento: '.$item_prod->datos_generales->fecha_nacimiento.'</li>';
        echo '<li>Donador de organos: '.$item_prod->datos_opciones->donador_organos.'</li>';
        echo '<li>Datos médicos: '.$item_prod->datos_opciones->datos_medicos.'</li>';
        echo '<li>Tipo de sangre: '.$item_prod->datos_generales->tipo_sangre.'</li>';
        echo '<li>Tipo de letra: '.$item_prod->disenopulsera->tipo_piedra.'</li>';
        echo '<li>Tipo de piedra: '.$item_prod->disenopulsera->tipo_letra.'</li>';
        echo '<li>Precio: '.moneda($item_prod->disenopulsera->precio_talla).' '.$item_prod->disenopulsera->moneda_precio.'</li>';
        echo '</ul>';
        $contador++;
    }
    echo '</div>';
}

function cyb_meta_piedra (){
    global $post;
    wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMeta_noncename2' );
    ?>
    <div id="meta_inner">
    <?php
    $piedra = get_post_meta($post->ID,'piedra',false);
    $c = 0;
    if (!empty($piedra)) {
        foreach( $piedra as $cklr2=>$track2 ) {
            foreach($track2 as $valor2) {
                printf('<p><input id="title" type="text" placeholder="Piedra" name="piedra[%1$s][nombrepiedra]" value="%2$s" /><span style="VERTICAL-ALIGN: super;" class="removeT"> <a href="javascript:void(0)" class="button">Remove</a></span></p>', $c, $valor2['nombrepiedra'], __('Eliminar'));
                $c = $c + 1;
            }
        }
    }
    ?>
    <span id="hereT"></span>
    <input type="button" class="addT" name="more" value="Agregar mas" />
    <script>
        var $ =jQuery.noConflict();
        $(document).ready(function() {
            var count = <?php echo $c; ?>;
            $(".addT").click(function() {
                count = count + 1;
                $('#hereT').append('<p><input id="title" placeholder="Piedra" type="text" name="piedra['+count+'][nombrepiedra]" value="" /><br><span style="VERTICAL-ALIGN: super;" class="removeT"> <a href="javascript:void(0)" class="button">Remove</a></span></p>' );
                return false;
            });
            $(".removeT").live('click', function() {
                $(this).parent().remove();
            });
        });
    </script>
    </div><?php
}

function cyb_meta_nosotros (){
    global $post;
    $nosotros = get_post_meta($post->ID,'nosotros',true);
    if(!empty($nosotros)){
        $nosotros_prod=$nosotros;
    }
    else{
        $nosotros_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= wp_editor($nosotros_prod,'nosotros',array('textarea_name'=>'nosotros'));
    $html .= '</div>';
    echo $html;
}

function cyb_meta_mision (){
    global $post;
    $mision = get_post_meta($post->ID,'mision',true);
    if(!empty($mision)){
        $mision_prod=$mision;
    }
    else{
        $mision_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= wp_editor($mision_prod,'mision',array('textarea_name'=>'mision'));
    $html .= '</div>';
    echo $html;
}

function cyb_meta_vision (){
    global $post;
    $vision  = get_post_meta($post->ID,'vision',true);
    if(!empty($vision)){
        $vision_prod=$vision;
    }
    else{
        $vision_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= wp_editor($vision_prod,'vision',array('textarea_name'=>'vision'));
    $html .= '</div>';
    echo $html;
}

function cyb_meta_valores (){
    global $post;
    $valores  = get_post_meta($post->ID,'valores',true);
    if(!empty($valores)){
        $valores_prod=$valores;
    }
    else{
        $valores_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= wp_editor($valores_prod,'valores',array('textarea_name'=>'valores'));
    $html .= '</div>';
    echo $html;
}


function cyb_meta_talllas (){
    global $post;
    $tallas_seccion3 = get_post_meta($post->ID,'tallas_seccion3',true);
    if(!empty($tallas_seccion3)){
        $tallas_seccion3_prod=$tallas_seccion3;
    }
    else{
        $tallas_seccion3_prod='';
    }

    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= wp_editor($tallas_seccion3_prod,'tallas',array('textarea_name'=>'tallas_seccion3'));
    $html .= '</div>';
    echo $html;
}

function cyb_meta_beneficios (){
    global $post;
    $beneficios = get_post_meta($post->ID,'beneficios_seccion2',true);
    if(!empty($beneficios)){
        $beneficios_prod=$beneficios;
    }
    else{
        $beneficios_prod='';
    }
    wp_nonce_field(plugin_basename(__FILE__), 'dynamicMeta_noncename2');

    $html = '<div>';
    $html .= wp_editor($beneficios_prod,'beneficios',array('textarea_name'=>'beneficios_seccion2'));
    $html .= '</div>';
    echo $html;
}

add_action( 'save_post', 'dynamic_save_postdata' );
function dynamic_save_postdata( $post_id ) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['dynamicMeta_noncename2'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename2'], plugin_basename( __FILE__ ) ) )
        return;


    $beneficios = $_POST['beneficios_seccion2'];
    update_post_meta($post_id,'beneficios_seccion2',$beneficios);

    $nosotros = $_POST['nosotros'];
    update_post_meta($post_id,'nosotros',$nosotros);

    $mision = $_POST['mision'];
    update_post_meta($post_id,'mision',$mision);

    $vision = $_POST['vision'];
    update_post_meta($post_id,'vision',$vision);

    $valores = $_POST['valores'];
    update_post_meta($post_id,'valores',$valores);

    $tallas_seccion3 = $_POST['tallas_seccion3'];
    update_post_meta($post_id,'tallas_seccion3',$tallas_seccion3);

    $piedra = $_POST['piedra'];
    update_post_meta($post_id,'piedra',$piedra);

    $galeria = $_POST['galeria_imagenes'];
    update_post_meta($post_id,'galeria_imagenes',$galeria);

    $status = $_POST['status'];
    update_post_meta($post_id,'estatus',$status);


    /*$nombre = $_POST['nombre'];
    update_post_meta($post_id,'nombre',$nombre);

    $apellido = $_POST['apellido'];
    update_post_meta($post_id,'apellido',$apellido);

    $contrasena = $_POST['contrasena'];
    update_post_meta($post_id,'contrasena',$contrasena);

    $country = $_POST['country'];
    update_post_meta($post_id,'country',$country);

    $state = $_POST['state'];
    update_post_meta($post_id,'state',$state);

    $ciudad = $_POST['ciudad'];
    update_post_meta($post_id,'ciudad',$ciudad);

    $direccion = $_POST['direccion'];
    update_post_meta($post_id,'direccion',$direccion);

    $codigo_postal = $_POST['codigo_postal'];
    update_post_meta($post_id,'codigo_postal',$codigo_postal);

    $email = $_POST['email'];
    update_post_meta($post_id,'email',$email);

    $telefono = $_POST['telefono'];
    update_post_meta($post_id,'telefono',$telefono);*/
}


function get_pagination($query){
    $big = 999999999; // need an unlikely integer


    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        //'base' => @add_query_arg('page','%#%'),
        'format' => '?paged=%#%',
        'prev_text'    => __('Anterior'),
        'current' => max(1,get_query_var('paged') ),
        'next_text'    => __('Siguiente'),
        'total' => $query->max_num_pages,
    ) );
}

add_action('wp_ajax_nopriv_addcliente', 'addcliente');
add_action('wp_ajax_addcliente', 'addcliente');
function addcliente(){

    $nombre = (isset($_REQUEST['nombre']) && $_REQUEST['nombre'] != "") ? $_REQUEST['nombre'] : "" ;
    $apellido= (isset($_REQUEST['apellido']) && $_REQUEST['apellido'] != "") ?  $_REQUEST['apellido'] : "" ;
    $contrasena= (isset($_REQUEST['contrasena']) && $_REQUEST['contrasena'] != "") ?  $_REQUEST['contrasena'] : "" ;
    $country = (isset($_REQUEST['country']) && $_REQUEST['country'] != "") ?  $_REQUEST['country'] : "" ;
    $state= (isset($_REQUEST['state']) && $_REQUEST['state'] != "") ?  $_REQUEST['state'] : "" ;
    $ciudad= (isset($_REQUEST['ciudad']) && $_REQUEST['ciudad'] != "") ?  $_REQUEST['ciudad'] : "" ;
    $direccion= (isset($_REQUEST['direccion']) && $_REQUEST['direccion'] != "") ?  $_REQUEST['direccion'] : "" ;
    $codigo_postal = (isset($_REQUEST['codigo_postal']) && $_REQUEST['codigo_postal'] != "") ?  intval($_REQUEST['codigo_postal']) : "" ;
    $email = (isset($_REQUEST['email']) && $_REQUEST['email'] != "") ?  $_REQUEST['email'] : "" ;
    $telefono= (isset($_REQUEST['telefono']) && $_REQUEST['telefono'] != "") ? $_REQUEST['telefono'] : "" ;

    $cliente = array(
        'post_title'    => $nombre.' '.$apellido,
        'post_type'     => 'distribuidores',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_category' => array()
    );

    $cliente_id=wp_insert_post( $cliente );

    update_post_meta($cliente_id,'nombre',$nombre);
    update_post_meta($cliente_id,'apellido',$apellido);
    update_post_meta($cliente_id,'contrasena',$contrasena);
    update_post_meta($cliente_id,'country',$country);
    update_post_meta($cliente_id,'state',$state);
    update_post_meta($cliente_id,'ciudad',$ciudad);
    update_post_meta($cliente_id,'direccion',$direccion);
    update_post_meta($cliente_id,'codigo_postal',$codigo_postal);
    update_post_meta($cliente_id,'email',$email);
    update_post_meta($cliente_id,'telefono',$telefono);

    echo json_encode(array('nombre' => $nombre, 'apellido' => $apellido, 'contrasena' => $contrasena , 'country' => $country, 'state' => $state, 'ciudad' => $ciudad, 'direccion' => $direccion,'email' => $email,'telefono' => $telefono));

    exit();
}

add_action('wp_ajax_nopriv_updatecliente', 'updatecliente');
add_action('wp_ajax_updatecliente', 'updatecliente');
function updatecliente(){

    $ID = (isset($_REQUEST['id']) && $_REQUEST['id'] != "") ? intval($_REQUEST['id']) : "" ;
    $nombre = (isset($_REQUEST['nombre']) && $_REQUEST['nombre'] != "") ? $_REQUEST['nombre'] : "" ;
    $apellido= (isset($_REQUEST['apellido']) && $_REQUEST['apellido'] != "") ?  $_REQUEST['apellido'] : "" ;
    $country = (isset($_REQUEST['country']) && $_REQUEST['country'] != "") ?  $_REQUEST['country'] : "" ;
    $state= (isset($_REQUEST['state']) && $_REQUEST['state'] != "") ?  $_REQUEST['state'] : "" ;
    $ciudad= (isset($_REQUEST['ciudad']) && $_REQUEST['ciudad'] != "") ?  $_REQUEST['ciudad'] : "" ;
    $direccion= (isset($_REQUEST['direccion']) && $_REQUEST['direccion'] != "") ?  $_REQUEST['direccion'] : "" ;
    $codigo_postal = (isset($_REQUEST['codigo_postal']) && $_REQUEST['codigo_postal'] != "") ?  intval($_REQUEST['codigo_postal']) : "" ;
    $email = (isset($_REQUEST['email']) && $_REQUEST['email'] != "") ?  $_REQUEST['email'] : "" ;
    $telefono= (isset($_REQUEST['telefono']) && $_REQUEST['telefono'] != "") ? $_REQUEST['telefono'] : "" ;

    $cliente = array(
        'ID'           => $ID,
        'post_title'    => $nombre.' '.$apellido,
        'post_type'     => 'distribuidores',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_category' => array()
    );

    $cliente_id=wp_update_post( $cliente );

    update_post_meta($ID,'nombre',$nombre);
    update_post_meta($ID,'apellido',$apellido);
    update_post_meta($ID,'country',$country);
    update_post_meta($ID,'state',$state);
    update_post_meta($ID,'ciudad',$ciudad);
    update_post_meta($ID,'direccion',$direccion);
    update_post_meta($ID,'codigo_postal',$codigo_postal);
    update_post_meta($ID,'email',$email);
    update_post_meta($ID,'telefono',$telefono);

    //echo json_encode(array('nombre' => $nombre, 'apellido' => $apellido, 'contrasena' => $contrasena , 'country' => $country, 'state' => $state, 'ciudad' => $ciudad, 'direccion' => $direccion,'email' => $email,'telefono' => $telefono));

    echo $cliente_id;

    exit();
}

add_action('wp_ajax_nopriv_login', 'login');
add_action('wp_ajax_login', 'login');
function login(){
    $email_recibido = (isset($_REQUEST['email']) && $_REQUEST['email'] != "") ? $_REQUEST['email'] : "" ;
    $password = (isset($_REQUEST['password']) && $_REQUEST['password'] != "") ? $_REQUEST['password'] : "" ;
    $args = array(
        'post_type' => 'distribuidores',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'DESC',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'email',
                'value' => "$email_recibido",
            ),
            array(
                'key' => 'contrasena',
                'value' => "$password",
            ),
        )
    );
    $query = new WP_Query($args);

    if($query->have_posts()){

        while ( $query->have_posts() ) : $query->the_post();
            $nombre = get_post_meta(get_the_ID(),'nombre',true);
            $apellido = get_post_meta(get_the_ID(),'apellido',true);
            $contrasena = get_post_meta(get_the_ID(),'contrasena',true);
            $country = get_post_meta(get_the_ID(),'country',true);
            $state = get_post_meta(get_the_ID(),'state',true);
            $ciudad = get_post_meta(get_the_ID(),'ciudad',true);
            $direccion = get_post_meta(get_the_ID(),'direccion',true);
            $codigo_postal = get_post_meta(get_the_ID(),'codigo_postal',true);
            $email = get_post_meta(get_the_ID(),'email',true);
            $telefono = get_post_meta(get_the_ID(),'telefono',true);
            echo json_encode(array('bandera'=> '1','ID'=> get_the_ID(),'nombre' => $nombre, 'apellido' => $apellido, 'contrasena' => $contrasena , 'country' => $country, 'state' => $state, 'ciudad' => $ciudad, 'direccion' => $direccion, 'cp'=>$codigo_postal, 'email' => $email,'telefono' => $telefono));
        endwhile;
    }else{
        echo json_encode(array('bandera'=> '2','ID'=> '0','nombre' => '', 'apellido' => '', 'contrasena' => '' , 'country' => '', 'state' => '', 'ciudad' => '', 'direccion' => '','email' => '','telefono' => ''));
    }

    exit();
}



add_action('wp_ajax_nopriv_addpedido', 'addpedido');
add_action('wp_ajax_addpedido', 'addpedido');
function addpedido(){

    $datos = (isset($_POST['datos']) && $_POST['datos'] != "") ? $_POST['datos'] : "" ;
    $folio = (isset($_REQUEST['folio']) && $_REQUEST['folio'] != "") ? $_REQUEST['folio'] : "" ;
    $total = (isset($_REQUEST['total']) && $_REQUEST['total'] != "") ? $_REQUEST['total'] : "" ;
    $nombre = (isset($_REQUEST['nombre']) && $_REQUEST['nombre'] != "") ? $_REQUEST['nombre'] : "" ;
    $calle = (isset($_REQUEST['calle']) && $_REQUEST['calle'] != "") ? $_REQUEST['calle'] : "" ;
    $num_exterior = (isset($_REQUEST['num_exterior']) && $_REQUEST['num_exterior'] != "") ? $_REQUEST['num_exterior'] : "" ;
    $num_interior = (isset($_REQUEST['num_interior']) && $_REQUEST['num_interior'] != "") ? $_REQUEST['num_interior'] : "" ;
    $cruzamiento = (isset($_REQUEST['cruzamientos']) && $_REQUEST['cruzamientos'] != "") ? $_REQUEST['cruzamientos'] : "" ;
    $cp = (isset($_REQUEST['cp']) && $_REQUEST['cp'] != "") ? $_REQUEST['cp'] : "" ;
    $colonia = (isset($_REQUEST['colonia']) && $_REQUEST['colonia'] != "") ? $_REQUEST['colonia'] : "" ;
    $ciudad = (isset($_REQUEST['ciudad']) && $_REQUEST['ciudad'] != "") ? $_REQUEST['ciudad'] : "" ;
    $telefono = (isset($_REQUEST['telefono']) && $_REQUEST['telefono'] != "") ? $_REQUEST['telefono'] : "" ;
    $celular = (isset($_REQUEST['celular']) && $_REQUEST['celular'] != "") ? $_REQUEST['celular'] : "" ;
    $email = (isset($_REQUEST['email']) && $_REQUEST['email'] != "") ? $_REQUEST['email'] : "" ;
    $pais = (isset($_REQUEST['pais']) && $_REQUEST['pais'] != "") ? $_REQUEST['pais'] : "" ;
    $estado = (isset($_REQUEST['estado']) && $_REQUEST['estado'] != "") ? $_REQUEST['estado'] : "" ;
    $datos_distribuidor = (isset($_REQUEST['datos_distribuidor']) && $_REQUEST['datos_distribuidor'] != "") ? $_REQUEST['datos_distribuidor'] : "" ;
    $currency_code = (isset($_REQUEST['currency_code']) && $_REQUEST['currency_code'] != "") ? $_REQUEST['currency_code'] : "" ;
    $new_array=array();
    $contador=0;



    foreach($datos as $item_prodcutos){
        $directorio = '/home/eternitta/public_html/wp-content/themes/eternitta/pulseras_generadas/';
        $valor=$item_prodcutos['disenopulsera']['imgpulsera'];
        $img = $valor;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $directorio .date('U').$contador. '.png';
        $success = file_put_contents($file, $data);
        $item_prodcutos['disenopulsera']['imgpulsera']=date('U').$contador.'.png';

        $item_prodcutos['datos_generales']['nombre']=$item_prodcutos['datos_generales']['nombre'];
        $item_prodcutos['datos_opciones']['datos_medicos']=$item_prodcutos['datos_opciones']['datos_medicos'];

        array_push($new_array,$item_prodcutos);

        $contador++;
    }

    $direccion_envio= array('nombre' => utf8_encode($nombre), 'calle'=> utf8_encode($calle), 'numero_exterior' => utf8_encode($num_exterior), 'numero_interior' => utf8_encode($num_interior), 'cruzamientos' => utf8_encode($cruzamiento), 'cp' => $cp, 'colonia' => $colonia, 'ciudad' => utf8_encode($ciudad), 'telefono' => $telefono, 'celular' => $celular, 'email'=>$email, 'pais' => utf8_encode($pais), 'estado'=>utf8_encode($estado));

    $pedidos = array(
        'post_title'    => 'Pedido #'.$folio,
        'post_type'     => 'pedidos',
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_category' => array()
    );

    $pedido_id=wp_insert_post( $pedidos );

    update_post_meta($pedido_id,'resumen_productos',json_encode($new_array));
    update_post_meta($pedido_id,'total_pedido',$total);
    update_post_meta($pedido_id,'fecha_pedido',date('d-m-Y'));
    update_post_meta($pedido_id,'estatus','Nuevo');
    update_post_meta($pedido_id,'direccion_envio',$direccion_envio);
    update_post_meta($pedido_id,'tipo_moneda',$currency_code);
    if($datos_distribuidor!=''){
        update_post_meta($pedido_id, 'datos_distribuidor', $datos_distribuidor);
    }else{
        update_post_meta($pedido_id, 'datos_distribuidor', '');
    }

    notifi_cliente($pedido_id);

    echo $pedido_id;
    //echo utf8_decode(json_encode($new_array));
    exit();
}

add_action('wp_ajax_nopriv_getatributos', 'getatributos');
add_action('wp_ajax_getatributos', 'getatributos');
function getatributos(){

    $idpedido = (isset($_REQUEST['pedidoID']) && $_REQUEST['pedidoID'] != "") ? $_REQUEST['pedidoID'] : "" ;
    $identificador = (isset($_REQUEST['identificador']) && $_REQUEST['identificador'] != "") ? $_REQUEST['identificador'] : "" ;

    $prod = get_post_meta($idpedido,'resumen_productos',true);
    $productosxpedido=json_decode($prod);

    echo json_encode(array('donador_organos' => $productosxpedido[$identificador]->datos_opciones->donador_organos,
        'datos_medicos'=>$productosxpedido[$identificador]->datos_opciones->datos_medicos,
        'tipo_piedra' => $productosxpedido[$identificador]->disenopulsera->tipo_piedra,
        'tipo_letra' => $productosxpedido[$identificador]->disenopulsera->tipo_letra,
        'tallas' => array('talla_s' => $productosxpedido[$identificador]->disenopulsera->tallas->talla_s,'talla_m' => $productosxpedido[$identificador]->disenopulsera->tallas->talla_m,'talla_l' => $productosxpedido[$identificador]->disenopulsera->tallas->talla_l,'talla_p' => $productosxpedido[$identificador]->disenopulsera->tallas->talla_p))
    );

    exit();
}


add_filter('manage_pedidos_posts_columns', 'ST4_columns_head');
add_action('manage_pedidos_posts_custom_column', 'ST4_columns_content', 1, 2);

function ST4_columns_head($defaults) {
    $defaults['fecha_pedido'] = 'Fecha del pedido';
    $defaults['total_pedido'] = 'Total del pedido';
    $defaults['estatus_pedido'] = 'Estatus del pedido';
    return $defaults;
}

// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'fecha_pedido') {
        $fecha_pedido =  get_post_meta($post_ID,'fecha_pedido',true);
        if ($fecha_pedido) {
            echo '<p>'.$fecha_pedido.'</p>';
        }
    }
    if ($column_name == 'total_pedido') {
        $total_pedido =  get_post_meta($post_ID,'total_pedido',true);
        $tipo_moneda= get_post_meta($post_ID,'tipo_moneda',true);
        if ($total_pedido) {
            echo '<p>'.moneda($total_pedido).' '.$tipo_moneda.'</p>';
        }
    }

    if ($column_name == 'estatus_pedido') {
        $estatus =  get_post_meta($post_ID,'estatus',true);
        if ($estatus) {
            echo '<p>'.$estatus.'</p>';
        }
    }
}

add_action('wp_ajax_nopriv_precioxtalla', 'precioxtalla');
add_action('wp_ajax_precioxtalla', 'precioxtalla');
function precioxtalla(){

    $talla_elegida=(isset($_REQUEST['newtalla']) && $_REQUEST['newtalla'] != "") ? $_REQUEST['newtalla'] : "" ;
    $current_moneda=(isset($_REQUEST['current_moneda']) && $_REQUEST['current_moneda'] != "") ? $_REQUEST['current_moneda'] : "" ;

    $talla=get_the_terms(25,'tallas');

    foreach($talla as $item_talla){
        if($item_talla->slug==$talla_elegida){
            $atributos = get_option( "tallas_$item_talla->term_id" );
            switch ($current_moneda) {
                case 'MXN':
                    $newprecio=$atributos['precio'];
                    break;
                case 'USD':
                    $newprecio=$atributos['preciousd'];
                    break;
                case 'EUR':
                    $newprecio=$atributos['precioeuro'];
                    break;
            }
        }
    }

    echo json_encode(array('precio'=>$newprecio));

    exit();
}


add_action('wp_ajax_nopriv_getpiedras', 'getpiedras');
add_action('wp_ajax_getpiedras', 'getpiedras');
function getpiedras(){

    $piedra = get_post_meta(25,'piedra',false);

    echo json_encode($piedra);

    exit();
}

function notifi_pagos($id){

    $direccion_envio=get_post_meta($id,'direccion_envio',true);

    add_filter( 'wp_mail_from', 'my_mail_from' );
    function my_mail_from( $email )
    {
        return "noreply@eternitta.com.mx";
    }

    add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
    function my_mail_from_name( $name )
    {
        return "Eternitta";
    }

    $to=$direccion_envio['email'];
    $subject2='Gracias por tu compra eternitta';
    $headers2 = 'Reply-to: noreply@eternitta.com.mx' . "\r\n";
    $message2 ='<div style="text-align: center; background: rgba(0, 0, 0, 0.7);; padding: 5px;"><img style="width: 160px;" src="http://eternitta.com.mx/wp-content/themes/eternitta/img/logo.png"></div><br/>';
    $message2.='<p style="text-align: center;">Has adquirido una pieza de Joyería en Acero inoxidable, manufacturada de forma artesanal y con los más altos estándares de calidad.</p>';
    $message2.='<p style="text-align: center;">Junto a tu producto recibirás un certificado de autenticidad Eternitta. Con tu número de folio y datos vitales.</p></div>';
    $message2.='<div style="text-align: center; background: #E6E6E6; padding: 5px; color: #00ACB4;">';
    $message2.='<p style="margin-bottom: 0px;">No responda este correo electrónico, ya que no estamos controlando esta bandeja de entrada. Para comunicarse con nosotros, haga clic <a style="color: #fff;" href="http://eternitta.com.mx/contacto">aquí</a>.</p>
            <p style="margin-bottom: 0px;">
                Copyright &copy; 20015-'.date('Y').' Eternitta. Todos los derechos reservados.
            </p></div>';
    add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

    wp_mail( $to, $subject2, $message2, $headers2);
}


function notifi_cliente($id){

    $direccion_envio=get_post_meta($id,'direccion_envio',true);
    $prod = get_post_meta($id,'resumen_productos',true);
    $productosxpedido=json_decode($prod);
    $total_global=0;

    add_filter( 'wp_mail_from', 'my_mail_from' );
    function my_mail_from( $email )
    {
        return "noreply@eternitta.com.mx";
    }

    add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
    function my_mail_from_name( $name )
    {
        return "Eternitta";
    }

    $to=$direccion_envio['email'];
    $subject='Pedido registrado Eternitta';
    $headers = 'Reply-to: noreply@eternitta.com.mx' . "\r\n";
    $message ='<div style="text-align: center; background: rgba(0, 0, 0, 0.7);; padding: 5px;"><img style="width: 160px;" src="http://eternitta.com.mx/wp-content/themes/eternitta/img/logo.png"></div><br/>';
    $message.='<div style="text-align: center; padding-bottom: 20px; color: #58585A;"><h2>Resumen de su pedido</h2>';
    $message.='<table>';
    $message.='<tr>';
    $message.='<thead>';
    $message.='<th>Imagen</th>';
    $message.='<th>Nombre</th>';
    $message.='<th>Talla elegida</th>';
    $message.='<th>Teléfono</th>';
    $message.='<th>Fecha de nacmiento</th>';
    $message.='<th>Donador de organos</th>';
    $message.='<th>Datos médicos</th>';
    $message.='<th>Tipo de sangre</th>';
    $message.='<th>Tipo de letra</th>';
    $message.='<th>Tipo de piedra</th>';
    $message.='<th>Precio</th>';
    $message.='</tr>';
    $message.='</thead>';
    $message.='<tbody style="text-align: center;">';
    foreach($productosxpedido as $item_prod){

        $total_global += $item_prod->disenopulsera->precio_talla;

        if($item_prod->disenopulsera->tallas->talla_s!='' && $item_prod->disenopulsera->tallas->talla_s > 0){
            $talla_elegida='S';
        }
        if($item_prod->disenopulsera->tallas->talla_m!='' && $item_prod->disenopulsera->tallas->talla_m > 0){
            $talla_elegida='M';
        }
        if($item_prod->disenopulsera->tallas->talla_l!='' && $item_prod->disenopulsera->tallas->talla_l > 0){
            $talla_elegida='L';
        }
        if($item_prod->disenopulsera->tallas->talla_p!='' && $item_prod->disenopulsera->tallas->talla_p > 0){
            $talla_elegida='P';
        }

        $url_imagen=get_template_directory_uri().'/pulseras_generadas/'.$item_prod->disenopulsera->imgpulsera;

        $message .='<tr>';
        $message .='<td><img style="width: 200px" src="'.$url_imagen.'"></td>';
        $message .='<td>'.$item_prod->datos_generales->nombre.'</td>';
        $message .='<td>'.$talla_elegida.'</td>';
        $message .='<td>'.$item_prod->datos_generales->telefono.'</td>';
        $message .='<td>'.$item_prod->datos_generales->fecha_nacimiento.'</td>';
        $message .='<td>'.$item_prod->datos_opciones->donador_organos.'</td>';
        $message .='<td>'.$item_prod->datos_opciones->datos_medicos.'</td>';
        $message .='<td>'.$item_prod->datos_generales->tipo_sangre.'</td>';
        $message .='<td>'.$item_prod->disenopulsera->tipo_letra.'</td>';
        $message .='<td>'.$item_prod->disenopulsera->tipo_piedra.'</td>';
        $message .='<td>'.$item_prod->disenopulsera->precio_talla.' '.$item_prod->disenopulsera->moneda_precio.'</td>';
        $message .='</tr>';
    }
    $message.='</tbody>';
    $message .= '<tfooter><td style="text-align: right;" colspan="10">Total: '.moneda($total_global).' '.$item_prod->disenopulsera->moneda_precio.'</td></tfooter>';
    $message.= '</table>';
    $message.='<p style="margin-bottom: 0px;">No responda este correo electrónico, ya que no estamos controlando esta bandeja de entrada. Para comunicarse con nosotros, haga clic <a style="color: #E6E6E6;" href="http://eternitta.com.mx/contacto">aquí</a>.</p>
            <p style="margin-bottom: 0px;">
                Copyright &copy; 20015-'.date('Y').' Eternitta. Todos los derechos reservados.
            </p></div>';
    add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
    wp_mail( $to, $subject, $message, $headers);


    $to2='info@eternitta.mx';
    $subject2='Nuevo pedido registrado eternitta';
    $headers2 = 'Reply-to: noreply@eternitta.com.mx' . "\r\n";
    $message2 ='<div style="text-align: center; background: rgba(0, 0, 0, 0.7);; padding: 5px;"><img style="width: 160px;" src="http://eternitta.com.mx/wp-content/themes/eternitta/img/logo.png"></div><br/>';
    $message2.='<div style="text-align: center; padding-bottom: 20px; color: #58585A;"><h2>Nuevo pedido desde eternitta</h2>';
    $message2.='<span>Se ha realizado un nuevo pedido haz click <a style="color: #999999;" href="http://eternitta.com.mx/wp-admin">aqui</a> para ver el pedido</span></div>';
    $message2.='<div style="text-align: center; background: #E6E6E6; padding: 5px; color: #00ACB4;">';
    $message2.='<p style="margin-bottom: 0px;">No responda este correo electrónico, ya que no estamos controlando esta bandeja de entrada. Para comunicarse con nosotros, haga clic <a style="color: #fff;" href="http://eternitta.com.mx/contacto">aquí</a>.</p>
            <p style="margin-bottom: 0px;">
                Copyright &copy; 20015-'.date('Y').' Eternitta. Todos los derechos reservados.
            </p></div>';
    add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

    wp_mail( $to2, $subject2, $message2, $headers2);
}

add_action('wp_ajax_nopriv_preciotallasxmoneda', 'preciotallasxmoneda');
add_action('wp_ajax_preciotallasxmoneda', 'preciotallasxmoneda');
function preciotallasxmoneda(){
    $currency_code=(isset($_REQUEST['currency_code']) && $_REQUEST['currency_code'] != "") ? $_REQUEST['currency_code'] : "" ;
    $listado_tallas=[];
    $args2 = array(
        'orderby'           => 'id',
        'order'             => 'DESC',
        'hide_empty'        => true,
        'exclude'           => array(),
        'exclude_tree'      => array(),
        'include'           => array(),
        'number'            => '',
        'fields'            => 'all',
        'slug'              => '',
        'parent'            => '',
        'hierarchical'      => true,
        'child_of'          => 0,
        'childless'         => false,
        'get'               => '',
        'name__like'        => '',
        'description__like' => '',
        'pad_counts'        => false,
        'offset'            => '',
        'search'            => '',
        'cache_domain'      => 'core'
    );
    $talla=get_terms( 'tallas',$args2 );
    $talla_ordenada=array_reverse($talla);
    foreach($talla_ordenada as $item_talla){
        $atributos = get_option( "tallas_$item_talla->term_id" );
        switch ($currency_code) {
            case 'MXN':
                $tallas=array('slug' => $item_talla->slug, 'precio' => $atributos['precio'], 'nombre'=>$atributos['nombre'], 'nombreus'=>$atributos['nombreus'], 'largo' => $atributos['largo'], 'ancho' =>$atributos['ancho']);
                break;
            case 'USD':
                $tallas=array('slug' => $item_talla->slug, 'precio' => $atributos['preciousd'],'nombre'=>$atributos['nombre'], 'nombreus'=>$atributos['nombreus'], 'largo' => $atributos['largo'], 'ancho' =>$atributos['ancho']);
                break;
            case 'EUR':
                $tallas=array('slug' => $item_talla->slug, 'precio' => $atributos['precioeuro'],'nombre'=>$atributos['nombre'], 'nombreus'=>$atributos['nombreus'], 'largo' => $atributos['largo'], 'ancho' =>$atributos['ancho']);
                break;
        }
        array_push($listado_tallas,$tallas);
    }

    echo  json_encode($listado_tallas);
    exit();
}

add_action('wp_ajax_nopriv_getreportedistribuidor', 'getreportedistribuidor');
add_action('wp_ajax_getreportedistribuidor', 'getreportedistribuidor');
function getreportedistribuidor(){

    $distribuidor=(isset($_REQUEST['distribuidor']) && $_REQUEST['distribuidor'] != "") ? $_REQUEST['distribuidor'] : "" ;
    $fecha_inicio=(isset($_REQUEST['fecha_inicial']) && $_REQUEST['fecha_inicial'] != "") ? $_REQUEST['fecha_inicial'] : "" ;
    $fecha_fin=(isset($_REQUEST['fecha_fin']) && $_REQUEST['fecha_fin'] != "") ? $_REQUEST['fecha_fin'] : "" ;
    $total_pedido='';
    $total_productos='';


    if($distribuidor !='' && $fecha_inicio=='' && $fecha_fin =='') {

        $args1 = array(
            'post_type' => 'pedidos',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'DESC'
        );

        $query = new WP_Query($args1);
        $contador=0;
        while ($query->have_posts()) : $query->the_post();
            $distribuidor_pedido=get_post_meta(get_the_ID(), 'datos_distribuidor');
            $datos_distribuidor=json_decode($distribuidor_pedido);
            $tipo_moneda=get_post_meta(get_the_ID(), 'tipo_moneda', true);
            if($distribuidor==$datos_distribuidor['nombre']){
                $total_pedido +=  get_post_meta(get_the_ID(),'total_pedido',true);
                switch ($tipo_moneda) {
                    case 'MXN':
                        $total_pedidomxn +=  get_post_meta(get_the_ID(),'total_pedido',true);
                        break;
                    case 'USD':
                        $total_pedidousd +=  get_post_meta(get_the_ID(),'total_pedido',true);
                        break;
                    case 'EUR':
                        $total_pedidoeur +=  get_post_meta(get_the_ID(),'total_pedido',true);
                        break;
                }
                $prod = get_post_meta(get_the_ID(),'resumen_productos',true);
                $total_productos+=count(json_decode($prod));
                $contador++;
            }

        endwhile;
    }

    if($distribuidor =='' && $fecha_inicio!='' && $fecha_fin !=''){

        $args2 = array(
            'post_type' => 'pedidos',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => 'fecha_pedido',
                    'value' => array($fecha_inicio,$fecha_fin),
                    'compare' => 'BETWEEN',
                ),
            )
        );

        $query = new WP_Query($args2);
        $contador=0;
        while ($query->have_posts()) : $query->the_post();
            $distribuidor_pedido=get_post_meta(get_the_ID(), 'datos_distribuidor');
            $tipo_moneda=get_post_meta(get_the_ID(), 'tipo_moneda', true);
            $datos_distribuidor=json_decode($distribuidor_pedido);

            switch ($tipo_moneda) {
                case 'MXN':
                    $total_pedidomxn +=  get_post_meta(get_the_ID(),'total_pedido',true);
                    break;
                case 'USD':
                    $total_pedidousd +=  get_post_meta(get_the_ID(),'total_pedido',true);

                    break;
                case 'EUR':
                    $total_pedidoeur +=  get_post_meta(get_the_ID(),'total_pedido',true);

                    break;
            }

            $prod = get_post_meta(get_the_ID(),'resumen_productos',true);
            $total_productos+=count(json_decode($prod));
            $contador++;
        endwhile;
    }

    if($distribuidor !='' && $fecha_inicio !='' && $fecha_fin !='') {

        $args3 = array(
            'post_type' => 'pedidos',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'DESC',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'fecha_pedido',
                    'value' => array($fecha_inicio,$fecha_fin),
                    'compare' => 'BETWEEN',
                ),
            )
        );

        $query = new WP_Query($args3);
        $contador=0;
        while ($query->have_posts()) : $query->the_post();
            $distribuidor_pedido=get_post_meta(get_the_ID(), 'datos_distribuidor');
            $datos_distribuidor=json_decode($distribuidor_pedido);

            if($distribuidor==$datos_distribuidor['nombre']){

                $tipo_moneda=get_post_meta(get_the_ID(), 'tipo_moneda', true);

                switch ($tipo_moneda) {
                    case 'MXN':
                        $total_pedidomxn +=  get_post_meta(get_the_ID(),'total_pedido',true);
                        break;
                    case 'USD':
                        $total_pedidousd +=  get_post_meta(get_the_ID(),'total_pedido',true);
                        break;
                    case 'EUR':
                        $total_pedidoeur +=  get_post_meta(get_the_ID(),'total_pedido',true);
                        break;
                }

                $prod = get_post_meta(get_the_ID(),'resumen_productos',true);
                $total_productos+=count(json_decode($prod));
                $contador++;
            }

        endwhile;
    }

    echo json_encode(array('cantidad_pedido' => $contador, 'total_pedidomxn'=>$total_pedidomxn, 'total_pedidousd'=> $total_pedidousd,'totaleur'=>$total_pedidoeur,'productos_totales' => $total_productos ));

    exit();
}

add_action('wp_ajax_nopriv_getpedidosxdistribuidor', 'getpedidosxdistribuidor');
add_action('wp_ajax_getpedidosxdistribuidor', 'getpedidosxdistribuidor');
function getpedidosxdistribuidor (){

    $pedidos=array();
    $distribuidor=(isset($_REQUEST['distribuidor']) && $_REQUEST['distribuidor'] != "") ? $_REQUEST['distribuidor'] : "" ;

    $args = array(
        'post_type' => 'pedidos',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'DESC'
    );

    $query = new WP_Query($args);

    if($query->have_posts()) {

        while ($query->have_posts()) : $query->the_post();
            $fecha_pedido = get_post_meta(get_the_ID(), 'fecha_pedido', true);
            $total_pedido = get_post_meta(get_the_ID(), 'total_pedido', true);
            $distribuidor_pedido = get_post_meta(get_the_ID(), 'datos_distribuidor', true);
            $estatus = get_post_meta(get_the_ID(), 'estatus', true);
            $datos_distribuidor = json_decode($distribuidor_pedido);
            $url=get_permalink(get_the_ID());
            if ($distribuidor_pedido!='' && $datos_distribuidor->nombre == $distribuidor) {
                $data = array('ID' => get_the_ID(), 'fecha' => $fecha_pedido, 'total' => $total_pedido,'estatus' => $estatus,'url'=>$url);
                array_push($pedidos, $data);
            }
        endwhile;
    }
    else{
        echo 'no se encontro nada';
    }

    echo json_encode($pedidos);

    exit();
}


function moneda($val)
{
    $symbol='$'; $r=2;

    $n = $val;
    $c = is_float($n) ? 1 : number_format($n,$r);
    $d = '.';
    $t = ',';
    $sign = ($n < 0) ? '-' : '';
    $i = $n=number_format(abs($n),$r);
    $j = (($j = count($i)) > 3) ? $j % 3 : 0;

    return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;
}

add_action('admin_footer', 'my_admin_footer_function');
function my_admin_footer_function() {
    echo '<div class="overlay_multiusos"></div><div class="modal_imagen"><a class="close_modalimagen" href="#">x</a><div class="popup_pulseraimagen"></div></div>';
}

function dameURL(){
$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
return $url;
}

add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
    add_menu_page( 'Reportes', 'Reportes', 'manage_options', 'reportes', 'reporte_distribuidor', '', 6 );
}

function reporte_distribuidor(){

    $args = array(
        'post_type' => 'distribuidores',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'DESC'
    );

    echo '<div class="reportes">';
    echo '<h1>Reportes</h1>';
    echo '<div class="filtros">';
    echo '<label>Distribuidores</label>';
    echo '<select class="distribuidor"><option value="">Seleccione una opción</option>';
    $query = new WP_Query($args);
    while ( $query->have_posts() ) : $query->the_post();
        echo '<option value="'.get_the_title().'">'.get_the_title().'</option>';
    endwhile;
    echo '</select>';
    echo '</div>';
    echo '<div class="filtros">';
    echo '<label>Fecha inicial</label>';
    echo '<input type="text" class="fecha_nacimiento fecha_inicial" name="fecha_inicial" value="">';
    echo '</div>';
    echo '<div class="filtros">';
    echo '<label>Fecha final</label>';
    echo '<input type="text" class="fecha_nacimiento fecha_fin" name="fecha_final" value="">';
    echo '</div>';
    echo '<input name="save" type="button" class="button button-primary button-large" id="buscar_reporte" value="Buscar">';
    echo '<div class="encabezado_resumen"></div>';
    echo '<div class="contenedor_resumen"></div>';
    echo '</div>';
}
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';