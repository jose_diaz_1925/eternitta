<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package eternitta
 */

get_header();

$args = array(
    'post_type' => 'post',
    'posts_per_page' => -1,
    'order' => 'ASC'
);
?>

	<div id="primary" class="single_articulo large-12 columns content-area">
        <div class="row">
            <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'template-parts/content', 'single' ); ?>

                <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;
                ?>

            <?php endwhile;  wp_reset_query() // End of the loop. ?>

            <h3 class="entry-title2">Artículos relacionados</h3>
            <div class="owl-carousel">
                <?php
                $query3 = new WP_Query($args);
                $contador3=0;
                if($query3->have_posts()){
                while($query3->have_posts()) : $query3->the_post();
                $feat_image3 = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
                ?>
                    <a href="<?php echo get_the_permalink(get_the_ID()) ?>">
                        <div class="testimonio_info">
                        <div class="img_destacada" ><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>"></div>
                        <?php the_title( '<p class="title-relacionado">', '</p>' ); ?>
                        <p class="description_articulorelacionado"><?php echo substr(strip_tags(get_the_content()),0,100); ?>...</p>
                    </div>
                    </a>
                <?php
                    $contador3++;
                endwhile;
                }
                wp_reset_query();
                ?>
            </div>
        </div>
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
