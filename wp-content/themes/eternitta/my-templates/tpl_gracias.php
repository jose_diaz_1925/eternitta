<?php
/* Template Name: gracias */

get_header();
?>
<section class="tenplate_gracias">
    <div class="row">
        <div class="center">
            <div class="text-center"><img src="<?php echo get_template_directory_uri() ?>/img/solologo.png" class="logo"></div>
            <h3 class="texto_gracias">GRaCIAS POR REALIZAR TU COMPRA CON NOSOTROS</h3>
        </div>
    </div>
</section>
<?php get_footer(); ?>