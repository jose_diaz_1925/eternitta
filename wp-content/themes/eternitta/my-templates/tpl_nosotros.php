<?php /* Template Name: nosotros */ ?>

<?php get_header() ?>

<section class="nosotros large-12 columns">
    <div class="over"></div>
    <div class="row contenido">
        <h3>Quiénes somos</h3>
        <div class="divisor"></div>
        <div class="texto"><?php echo  get_post_meta(get_the_ID(),'nosotros',true); ?></div>
        <h3>Misión</h3>
        <div class="divisor"></div>
        <div class="texto"><?php echo  get_post_meta(get_the_ID(),'mision',true); ?></div>
        <h3>Visión</h3>
        <div class="divisor"></div>
        <div class="texto"><?php echo  get_post_meta(get_the_ID(),'vision',true); ?></div>
        <h3>Valores</h3>
        <div class="divisor"></div>
        <div class="texto valores"><?php echo  get_post_meta(get_the_ID(),'valores',true); ?></div>
    </div>
</section>

<?php get_footer(); ?>