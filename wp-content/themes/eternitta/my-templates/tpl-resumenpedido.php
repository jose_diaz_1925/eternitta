<?php
/* Template Name: Resumen pedido */

get_header(); ?>

<div class="small-12 medium-12 large-12 columns resumen_pedido">
    <div class="row">
        <h2 class="titulo_blog"><?php echo get_the_title($_GET['id']); ?></h2>
        <div class="divisor"></div>
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3 articulos">
            <?php
            $prod = get_post_meta($_GET['id'],'resumen_productos',true);
            $productosxpedido=json_decode($prod);

            foreach($productosxpedido as $item_resumen){

                if($item_resumen->disenopulsera->tallas->talla_s!='' && $item_resumen->disenopulsera->tallas->talla_s > 0){
                    $talla_elegida='S';
                }
                if($item_resumen->disenopulsera->tallas->talla_m!='' && $item_resumen->disenopulsera->tallas->talla_m > 0 ){
                    $talla_elegida='M';
                }
                if($item_resumen->disenopulsera->tallas->talla_l!='' && $item_resumen->disenopulsera->tallas->talla_l > 0){
                    $talla_elegida='L';
                }
                if($item_resumen->disenopulsera->tallas->talla_p!='' && $item_resumen->disenopulsera->tallas->talla_p > 0){
                    $talla_elegida='P';
                }
            ?>
                <li>
                    <div class="contenido_foto"><div style="background-image: url('<?php echo get_template_directory_uri().'/pulseras_generadas/'.$item_resumen->disenopulsera->imgpulsera; ?>')" class="img_articulo"></div></div>
                    <p class="datos_pulsera">Nombre: <?php echo $item_resumen->datos_generales->nombre ?></p>
                    <p class="datos_pulsera">Talla: <?php echo $talla_elegida ?></p>
                    <p class="datos_pulsera">Precio: <?php echo moneda($item_resumen->disenopulsera->precio_talla).' '.$item_resumen->disenopulsera->moneda_precio ?></p>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
