<?php /* Template Name: home */ ?>

<?php
$args = array(
    'post_type' => 'productos',
    'posts_per_page' => -1,
    'orderby'=> 'ID',
    'order' => 'asc',
    'p' => 25,
);
?>

<?php get_header() ?>
<div class="home large-12 columns">
	<section class="seccion1">
		<div class="over"></div>
        <div class="center">
            <img src="<?php echo get_template_directory_uri() ?>/img/logo.png" class="logo">
                <div class="divisor"></div>
            <div class="descripcion_eternita">
                <?php
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;
                ?>
            </div>
        </div>
        <!--<a class="arrow_down" href="#2"><img src="<?php echo get_template_directory_uri() ?>/img/down.png" class=""></a>-->
        <div class="botones_home">
            <div class="comprar_btn"><a href="#2">Beneficios</a></div>
            <div class="comprar_btn"><a href="#3">Comprar</a></div>
        </div>
	</section>
	<section id="2">
        <div class="over"></div>
        <div class="content2">
            <div class="row">
                <h3 class="titulo">Beneficios</h3>
                <div class="divisor"></div>
            <?php $beneficios = get_post_meta($post->ID,'beneficios_seccion2',true); ?>
            <?php echo $beneficios; ?>
            </div>
        </div>
	</section>
	<section id="3">
        <div class="row bloque1">
            <h3 class="titulo">Elije tu talla y cantidad</h3>
            <div class="divisor"></div>
            <?php $tallas_seccion3 = get_post_meta($post->ID,'tallas_seccion3',true); ?>
            <div class="contenido"><?php echo $tallas_seccion3; ?></div>
        </div>
        <div class="aviso_seleccion">
            <p class="texto_aviso">Para inicar tu pedido es necesario que elijas un tipo de moneda</p>
            <a class="select_moneda" href="javascript:void(0)">Seleccionar tipo de moneda</a>
        </div>
        <div class="row">
            <?php
            $args2 = array(
                'orderby'           => 'id',
                'order'             => 'DESC',
                'hide_empty'        => true,
                'exclude'           => array(),
                'exclude_tree'      => array(),
                'include'           => array(),
                'number'            => '',
                'fields'            => 'all',
                'slug'              => '',
                'parent'            => '',
                'hierarchical'      => true,
                'child_of'          => 0,
                'childless'         => false,
                'get'               => '',
                'name__like'        => '',
                'description__like' => '',
                'pad_counts'        => false,
                'offset'            => '',
                'search'            => '',
                'cache_domain'      => 'core'
            );
            ?>
            <?php $talla=get_terms( 'tallas',$args2 ); ?>
            <?php $talla_ordenada=array_reverse($talla); //print_r($talla); ?>
            <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4 contenido_tallas">

            </ul>
            <a href="<?php echo dameURL(); ?>/pedido"><div class="btn_listo">Ver carrito</div></a>
        </div>
	</section>
        <?php $query = new WP_Query($args);
        while ( $query->have_posts() ) : $query->the_post(); ?>
            <?php $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
    <section id="4" style="background-image: url('<?php echo $imgdestacada; ?>')">
        <!--<div class="disabled_seccion">
            <h2>
                Seleccione una talla primero
            </h2>
        </div>
        <div class="small-12 medium-6 large-8 columns contenedor_brasalete">
           <p class="nombre_brasalete">Tu nombre aqui</p>
        </div>
        <div class="small-12 medium-6 large-4 columns contenedor_datos">
            <h3 class="titulo_brasalete">Personaliza tu brazalete</h3>
            <div class="divisor"></div>
            <div class="info_brasalete">
                <label>Nombre</label>
                <input type="text" class="nombre" name="nombre" value="">
                <label>Telefono</label>
                <input type="text" class="telefono" name="telefono" value="">
                <label>Fecha de nacimiento</label>
                <input type="text" class="fecha_nacimiento" name="nombre" value="">
                <label>Tipo de sangre</label>
                <select class="tipo_sangre">
                    <option value="">Seleccione una opción</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B+">B+</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                </select>
                <h4>Datos opcionales:</h4>
                <label>Donador de organos</label>
                <select class="donador">
                    <option value="">Seleccione una opción</option>
                    <option value="si">Si</option>
                    <option value="no">No</option>
                </select>
                <label>Dato medico (enfermedades / alergía )</label>
                <textarea name="datos_medico"></textarea>
                <label>Tipo de letra</label>
                <select class="tipo_letra">
                    <option value="">Selecciona una opción</option>
                    <option value="bookman">Bookman</option>
                    <option value="daniela">Daniela</option>
                    <option value="brush">Brush</option>
                    <option value="monotype">Monotype</option>
                    <option value="steelfishRg">SteelfishRg</option>
                    <option value="malbeckregular">Malbeck-Regular</option>
                    <option value="times">Times</option>
                </select>
                <label>Tipo de piedra</label>
                <?php $piedra = get_post_meta($post->ID,'piedra',false); ?>
                <select class="piedra">
                    <?php
                    foreach( $piedra as $cklr2=>$track2 ) {
                        foreach($track2 as $valor2) {
                            //printf('<p><input id="title" type="text" placeholder="Piedra" name="piedra[%1$s][nombrepiedra]" value="%2$s" /><span style="VERTICAL-ALIGN: super;" class="removeT"> <a href="javascript:void(0)" class="button">Remove</a></span></p>', $c, $valor2['nombrepiedra'], __('Eliminar'));
                            echo '<option value="'.$valor2['nombrepiedra'].'">'.$valor2['nombrepiedra'].'</option>';
                        }
                    }
                    ?>
                </select>
                <div class="contenedor_btncompra">
                    <a class="btn_pedido" href="javascript:void(0);">Guardar</a>
                </div>
            </div>
        </div>-->
    </section>
        <?php endwhile ?>
    <div class="modal_personaliza">
        <!--<a class="close_personaliza" href="javascript:void(0);"><div class="glyph-icon flaticon-delete30"></div></a>-->
        <div class="small-12 medium-12 large-12 columns contenedor_datos">
            <input type="hidden" class="precio_talla" name="precio_talla" value="">
            <h3 class="titulo_brasalete">Personaliza tu brazalete</h3>
            <div class="divisor"></div>

            <div class="large-6 columns img_brasalete">
                <?php $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
                <div class="container_pulsera">
                    <img alt="Pulsera eternitta" src="<?php echo $imgdestacada ?>">
                    <p class="nombre_pulseradiseno" style="color: #fff;">Tu nombre aqui</p>
                </div>
                <?php //$galeria=get_post_meta($post->ID,'galeria_imagenes',true) ?>
                <?php //$data = json_decode($galeria); ?>
                <!--<div class="owl-carousel_pedido">-->
                    <?php //foreach($data as $item_gal){ ?>
                        <!--<img src="<?php //echo $item_gal->data->urlarchivo ?>" class="logo">-->
                    <?php //} ?>
                <!--</div>-->
            </div>

            <div class="large-6 columns">
            <div class="small-12 medium-6 large-6 columns info_brasalete">
                <h4>Datos obligatorios:</h4>
                <label class="inline2">Nombre</label>
                <small class="aviso_small">Limite 30 caracteres</small>
                <input type="text" class="nombre valida_nombre" name="nombre" value="" maxlength="30">
                <label>Telefono</label>
                <input type="text" class="telefono" name="telefono" value="">
                <label>Fecha de nacimiento</label>
                <input type="text" class="fecha_nacimiento" name="nombre" value="">
                <label>Tipo de sangre</label>
                <select class="tipo_sangre">
                    <option value="">Seleccione una opción</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B+">B+</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                </select>
            </div>
            <div class="small-12 medium-6 large-6 columns info_brasalete">
                <h4>Datos opcionales:</h4>
                <label>Donador de organos</label>
                <select class="donador">
                    <option value="">Seleccione una opción</option>
                    <option value="si">Si</option>
                    <option value="no">No</option>
                </select>
                <label>Dato medico (enfermedades / alergía )</label>
                <small class="aviso_small">Limite 140 caracteres</small>
                <textarea class="datos_medico" name="datos_medico" maxlength="140"></textarea>
                <label>Tipo de letra</label>
                <select class="tipo_letra">
                    <option value="">Selecciona una opción</option>
                    <option value="bookman">Bookman</option>
                    <option value="danielaregular">Daniela</option>
                    <option value="denseregular">Dense</option>
                    <option value="brushbtregular">Brush</option>
                    <option value="applechancerychancery">Apple chancery</option>
                    <option value="steelfishregular">SteelfishRg</option>
                    <option value="malbeckregular">Malbeck-Regular</option>
                    <option value="timesregular">Times</option>
                </select>
                <label>Tipo de piedra</label>
                <?php $piedra = get_post_meta($post->ID,'piedra',false); ?>
                <select class="piedra">
                    <option value="">Selecciona una opción</option>
                    <?php
                    foreach( $piedra as $cklr2=>$track2 ) {
                        foreach($track2 as $valor2) {
                            //printf('<p><input id="title" type="text" placeholder="Piedra" name="piedra[%1$s][nombrepiedra]" value="%2$s" /><span style="VERTICAL-ALIGN: super;" class="removeT"> <a href="javascript:void(0)" class="button">Remove</a></span></p>', $c, $valor2['nombrepiedra'], __('Eliminar'));
                            echo '<option value="'.$valor2['nombrepiedra'].'">'.$valor2['nombrepiedra'].'</option>';
                        }
                    }

                    ?>
                </select>
            </div>

                <div class="text-right">
                    <div class="contenedor_btncompra">
                        <a class="btn_pedido" href="javascript:void(0);">Guardar</a>
                    </div>
                    <div class="contenedor_btncancelar">
                        <a class="close_personaliza btn_cancelar" href="javascript:void(0);">Cancelar</a>
                    </div>
                    <!--<div class="canvashere contenedor_btncompra">
                        <a class="#">Canvas here</a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>