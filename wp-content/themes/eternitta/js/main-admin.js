jQuery(document).ready(function(){

    jQuery( ".fecha_nacimiento" ).datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-70:+0"
    });

    jQuery('#buscar_reporte').click(function(){

        var distribuidor=jQuery('.distribuidor').val();
        var fecha_inicial=jQuery('.fecha_inicial').val();
        var fecha_fin=jQuery('.fecha_fin').val();

        jQuery.ajax({
            url : '/wp-admin/admin-ajax.php',
            timeout: 8000,
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'getreportedistribuidor',
                distribuidor: distribuidor,
                fecha_inicial: fecha_inicial,
                fecha_fin: fecha_fin
            },
            beforeSend: function(){
            },
            success: function (data) {
                var totalusd=parseInt(data.total_pedidousd);
                var totalmxn=parseInt(data.total_pedidomxn);
                var totaleur=parseInt(data.totaleur);
                if(fecha_inicial !='' && fecha_fin !=''){
                    jQuery('.encabezado_resumen').html('<div>Periodo de: '+fecha_inicial+' hasta '+fecha_fin+'</div>');
                }
                jQuery('.contenedor_resumen').html('<div><span class="titulo_resumen">Cantidad total de pedido realizados:</span><span class="dato">'+data.cantidad_pedido+'</span></div><div><span class="titulo_resumen">Total de productos pedidos:</span><span class="dato">'+data.productos_totales+'</span></div><div><span class="titulo_resumen">Total de pedido realizados en pesos:</span><span class="dato">'+totalmxn.formatMoney()+'</span></div><div><span class="titulo_resumen">Total de pedido realizados en dollares:</span><span class="dato">'+totalusd.formatMoney()+'</span></div><div><span class="titulo_resumen">Total de pedido realizados en euro:</span><span class="dato">'+totaleur.formatMoney()+'</span></div>');

            },
            error: function (err) {
            }
        })
    })

    jQuery('.img_pulsera').on('click','img',function(){
        var srcimg=jQuery(this).data('urlimg');
        jQuery('.overlay_multiusos').fadeIn();
        jQuery('.modal_imagen').show();
        jQuery('.popup_pulseraimagen').html('<img src="'+srcimg+'">');
    })

    jQuery('.close_modalimagen').click(function(){
        jQuery('.modal_imagen').hide();
        jQuery('.overlay_multiusos').fadeOut();
    })

    jQuery('.moreprodpedido').click(function(){

        var idpedido=jQuery(this).data('idpedido');
        var ididentificador=jQuery(this).data('identificador');
        jQuery.ajax({
            url : '/eternita/wp-admin/admin-ajax.php',
            timeout: 8000,
            type: 'POST',
            data: {
                action: 'getatributos',
                pedidoID: idpedido,
                identificador: ididentificador
            },
            beforeSend: function(){
            },
            success: function (data) {
                console.log(data);
                jQuery('.details').html('<p>Hola</p>');

            },
            error: function (err) {
            }
        })

    })
    var formfield;
    /* user clicks button on custom field, runs below code that opens new window */
    var custom_uploader;
    var files = [];
    var images_re = [];
    var currentImages;

    jQuery('.onetarek-upload-button').click(function(e) {

        var contador=parseInt(jQuery(this).data('contador'));
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Agregar imagen',
            button: {
                text: 'Seleciona una imagen'
            },
            multiple: true
        });
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').toJSON();
            //console.log(attachment)

            var value = jQuery('#image_location').val();

            if(value != ""){
                currentImages = JSON.parse(value);
                files = currentImages;
            }

            jQuery.each( attachment, function( i, val ) {
                var sizes = {
                    namearchivo : val.name,
                    data : {
                        tipoarchivo: val.subtype,
                        icon: val.icon,
                        urlarchivo: val.url,
                        filename: val.filename
                    }
                };
                //create images sizes array
                var posicion= parseInt(contador)+parseInt(i);
                files.push(sizes);
                jQuery('.items').append('<div class="img_'+posicion+'" style="float: left; overflow: hidden; position: relative; width: 15%;display: inline;margin: 10px;"><img style="    width: 100%;" src="'+val.url+'" ><p>Archivo: '+val.name+'</p><a data-position="'+posicion+'" class="remove_img" name="'+ val.name +'" style="position: absolute;right: 0px;top: 0;background: rgba(0,0,0,0.7);padding: 5px;color: #fff;text-decoration: none;" >Borrar</a></div>');

            });
            jQuery('#image_location').val(JSON.stringify(files));
            console.log(files);
            jQuery(".remove_img").click(function(){

                var position=parseInt($(this).data('position'));
                //images.splice(position,1);
                delete files[position]
                jQuery('.img_' + position + '').hide('slow');
                jQuery('#image_location').val(JSON.stringify(files));
                console.log(files)
                /*$.each( images, function( i, val2 ) {
                 //if(images.image!=undefined) {
                 console.log(val2);
                 if (name_img == val2.image && position == i) {
                 images.splice(position, 1);
                 $('.img_' + i + '').hide('slow');
                 console.log(images);
                 if(images.image!=undefined) {
                 var sizes = {
                 image: val2.name,
                 sizes: {
                 thumbnail: val2.sizes.thumbnail.url,
                 medium: val2.sizes.medium.url,
                 full: val2.sizes.full.url
                 }
                 };

                 images_re.push(sizes);
                 }
                 $('#image_location').val(JSON.stringify(images_re));
                 }
                 });*/
            });
            //console.log(images)
        });
        //Open the uploader dialog
        custom_uploader.open();
    });

    jQuery(".remove_img").click(function(){
        var arr = JSON.parse(jQuery('#image_location').val());
        var position=parseInt(jQuery(this).data('position'));
        //images.splice(position,1);
        delete arr[position]
        jQuery('.img_' + position + '').hide('slow');
        jQuery('#image_location').val(JSON.stringify(arr));
        console.log(arr)
        /*$.each( images, function( i, val2 ) {
         //if(images.image!=undefined) {
         console.log(val2);
         if (name_img == val2.image && position == i) {
         images.splice(position, 1);
         $('.img_' + i + '').hide('slow');
         console.log(images);
         if(images.image!=undefined) {
         var sizes = {
         image: val2.name,
         sizes: {
         thumbnail: val2.sizes.thumbnail.url,
         medium: val2.sizes.medium.url,
         full: val2.sizes.full.url
         }
         };

         images_re.push(sizes);
         }
         $('#image_location').val(JSON.stringify(images_re));
         }
         });*/
    });


    /*
     Please keep these line to use this code snipet in your project
     Developed by oneTarek http://onetarek.com
     */
    //adding my custom function with Thick box close function tb_close() .
    window.old_tb_remove = window.tb_remove;
    window.tb_remove = function() {
        window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
        formfield=null;
    };

    // user inserts file into post. only run custom if user started process using the above process
    // window.send_to_editor(html) is how wp would normally handle the received data

    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor = function(html){
        if (formfield) {
            fileurl = jQuery('img',html).attr('src');
            jQuery(formfield).val(fileurl);
            tb_remove();
        } else {
            window.original_send_to_editor(html);
        }
    };

})

Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};