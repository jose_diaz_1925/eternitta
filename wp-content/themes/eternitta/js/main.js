var talla_chico=0;
var talla_mediano=0;
var talla_grande=0;
var talla_plus=0;
var click_carrito=0;


jQuery(document).ready(function(){

    var bodyclass= jQuery('body').hasClass('page-id-32');
    var bodyclasstag= jQuery('body').hasClass('tag');
    var bodyclasscategory= jQuery('body').hasClass('category');
    var bodyclasswisijap= jQuery('body').hasClass('single-wysijap');
    var bodyclasshome= jQuery('body').hasClass('home');
    var bodyclasspanel= jQuery('body').hasClass('page-id-221');

    if(bodyclass){
        var iconos=jQuery('.share-facebook');
        for(var z=0; z < iconos.length; z++){
            var iconosmalo=jQuery(iconos[z]).find('a');
            jQuery(iconosmalo[0]).css('display','none');
        }
    }

    if(bodyclasscategory){
        var iconos=jQuery('.share-facebook');
        for(var z=0; z < iconos.length; z++){
            var iconosmalo=jQuery(iconos[z]).find('a');
            jQuery(iconosmalo[0]).css('display','none');
        }
    }

    if(bodyclasstag){
        var iconos=jQuery('.share-facebook');
        for(var z=0; z < iconos.length; z++){
            var iconosmalo=jQuery(iconos[z]).find('a');
            jQuery(iconosmalo[0]).css('display','none');
        }
    }
    if(bodyclasswisijap){
        jQuery('.fecha_publicado').hide();
        jQuery('.entry-title2').text('Quisas te interesen estos artículos');
    }


    if(localStorage.getItem('currency_code')){
        jQuery('.current_moneda').html(localStorage.getItem('currency_code'));
        jQuery('.aviso_seleccion').hide();

        if(localStorage.getItem('currency_code')!='MXN'){
            jQuery('.compropago').hide();
            jQuery('.paypal').css('width','100% !important');
        }

        if(bodyclasshome) {
            var tipo_moneda = localStorage.getItem('currency_code');
            var listado_tallas2 = '';
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'preciotallasxmoneda',
                    currency_code: tipo_moneda
                },
                beforeSend: function () {
                    jQuery('.overlay_loader').show();
                },
                success: function (data) {
                    jQuery('.overlay_loader').hide();
                    jQuery('.tipo_moneda').hide();
                    jQuery('.overlay_multiusos2').fadeOut();
                    for (var e = 0; e < data.length; e++) {
                        var precio = parseInt(data[e].precio);
                        if(getUrlParameter('lang')=='en'){
                            var nombretra=data[e].nombreus
                        }else{
                           var nombretra=data[e].nombre
                        }
                        listado_tallas2 += '<li><div id="tallas" data-talla="' + data[e].slug + '" class="boton_talla"><p data-precio="' + data[e].precio + '" data-talla="' + data[e].slug + '" class="talla">' + data[e].slug + '</p></div><p class="name_talla">' + nombretra + '</p><p class="descripcion_talla"><small>$' + precio + '.00 '+localStorage.getItem('currency_code')+'</small></p><p class="descripcion_talla"><small>' + data[e].largo + ' cm de largo</small></p><p class="descripcion_talla"><small>' + data[e].ancho + ' cm de largo</small></p></li>';
                        precio = '';
                    }
                    console.log(data);
                    jQuery('.contenido_tallas').html(listado_tallas2);
                    localStorage.setItem('currency_code', tipo_moneda);
                    jQuery('.current_moneda').html(localStorage.getItem('currency_code'));
                },
                error: function (err) {
                    jQuery('.overlay_loader').hide();
                    if(getUrlParameter('lang')=='en'){
                        mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                    }else{
                        mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                    }
                }
            })
        }
    }else{
        //jQuery('.tipo_moneda').show();
        //jQuery('.overlay_multiusos2').fadeIn();
        //jQuery('.bloque1').hide();

    }

    jQuery('.select_moneda').click(function(){
        jQuery('.tipo_moneda').show();
        jQuery('.overlay_multiusos2').fadeIn();

    })

    jQuery('.current_moneda').click(function(){
        jQuery('.tipo_moneda_edit').show();
        var moneda=localStorage.getItem('currency_code');
        jQuery('.tipo_moneda_edit').find('.monedasupdate').val(moneda);
        jQuery('.overlay_multiusos2').fadeIn();
    })

    jQuery('.btnlatermoneyupdatecancel').click(function(){
        jQuery('.tipo_moneda_edit').hide();
        jQuery('.overlay_multiusos2').fadeOut();
    })

    jQuery('.btnlatermoneyupdate').click(function(){
            var tipo_moneda = jQuery('.monedasupdate').val();
            var listado_tallas = '';
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'preciotallasxmoneda',
                    currency_code: tipo_moneda
                },
                beforeSend: function () {
                    jQuery('.overlay_loader').show();
                },
                success: function (data) {
                    jQuery('.overlay_loader').hide();
                    jQuery('.tipo_moneda_edit').hide();
                    jQuery('.overlay_multiusos2').fadeOut();
                    jQuery('.aviso_seleccion').hide();
                    if(bodyclasshome) {
                        for (var e = 0; e < data.length; e++) {
                            var precio = parseInt(data[e].precio);
                            listado_tallas += '<li><div id="tallas" data-talla="' + data[e].slug + '" class="boton_talla"><p data-precio="' + data[e].precio + '" data-talla="' + data[e].slug + '" class="talla">' + data[e].slug + '</p></div><p class="name_talla">' + data[e].nombre + '</p><p class="descripcion_talla"><small>$' + precio + '.00 ' + tipo_moneda + '</small></p><p class="descripcion_talla"><small>' + data[e].largo + ' cm de largo</small></p><p class="descripcion_talla"><small>' + data[e].ancho + ' cm de largo</small></p></li>';
                            precio = '';
                        }
                        jQuery('.contenido_tallas').html(listado_tallas);
                    }
                    localStorage.setItem('currency_code', tipo_moneda);
                    localStorage.setItem('tallas',JSON.stringify(data));
                    jQuery('.current_moneda').html(localStorage.getItem('currency_code'));

                    var datapedidos=JSON.parse(localStorage.getItem('eternita_pedido'));
                    var datatalla=JSON.parse(localStorage.getItem('tallas'));

                    for(var m=0; m < datapedidos.length; m++){
                        datapedidos[m].disenopulsera.moneda_precio=tipo_moneda;
                        if(datapedidos[m].disenopulsera.tallas.talla_s!=0){
                            for(var e=0; e < datatalla.length; e++){
                                if('talla_'+datatalla[e].slug == 'talla_s'){
                                    var nuevoprecio=datatalla[e].precio
                                }
                            }
                        }

                        if(datapedidos[m].disenopulsera.tallas.talla_m!=0){
                            for(var e=0; e < datatalla.length; e++){
                                if('talla_'+datatalla[e].slug == 'talla_m'){
                                    var nuevoprecio=datatalla[e].precio
                                }
                            }
                        }

                        if(datapedidos[m].disenopulsera.tallas.talla_l!=0){
                            for(var e=0; e < datatalla.length; e++){
                                if('talla_'+datatalla[e].slug == 'talla_l'){
                                    var nuevoprecio=datatalla[e].precio
                                }
                            }
                        }

                        if(datapedidos[m].disenopulsera.tallas.talla_p!=0){
                            for(var e=0; e < datatalla.length; e++){
                                if('talla_'+datatalla[e].slug == 'talla_p'){
                                    var nuevoprecio=datatalla[e].precio
                                }
                            }
                        }
                        datapedidos[m].disenopulsera.precio_talla=nuevoprecio;
                    }
                    localStorage.setItem('eternita_pedido', JSON.stringify(datapedidos));
                    location.reload();
                },
                error: function (err) {
                    jQuery('.overlay_loader').hide();
                    if (getUrlParameter('lang') == 'en') {
                        mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                    } else {
                        mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                    }
                }
            })
    })

    jQuery('.btncurrentmoney').click(function(){
        var tipo_moneda=jQuery('.monedas').val();
        var listado_tallas='';
        jQuery.ajax({
            url : '/wp-admin/admin-ajax.php',
            timeout: 8000,
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'preciotallasxmoneda',
                currency_code: tipo_moneda
            },
            beforeSend: function(){
                jQuery('.overlay_loader').show();
            },
            success: function (data) {
                jQuery('.overlay_loader').hide();
                jQuery('.tipo_moneda').hide();
                jQuery('.overlay_multiusos2').fadeOut();
                jQuery('.aviso_seleccion').hide();
                for(var e=0; e < data.length; e++){
                    var precio=parseInt(data[e].precio);
                    listado_tallas+='<li><div id="tallas" data-talla="'+data[e].slug+'" class="boton_talla"><p data-precio="'+data[e].precio+'" data-talla="'+data[e].slug+'" class="talla">'+data[e].slug+'</p></div><p class="name_talla">'+data[e].nombre+'</p><p class="descripcion_talla"><small>$'+precio+'.00 '+tipo_moneda+'</small></p><p class="descripcion_talla"><small>'+data[e].largo+' cm de largo</small></p><p class="descripcion_talla"><small>'+data[e].ancho+' cm de largo</small></p></li>';
                    precio='';
                }
                jQuery('.contenido_tallas').html(listado_tallas);
                localStorage.setItem('tallas',JSON.stringify(data));
                localStorage.setItem('currency_code',tipo_moneda);
                jQuery('.current_moneda').html(localStorage.getItem('currency_code'));
            },
            error: function (err) {
                jQuery('.overlay_loader').hide();
                if(getUrlParameter('lang')=='en'){
                    mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                }else{
                    mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                }
            }
        })
    })

    jQuery('.btnlatermoney').click(function(){
        jQuery('.tipo_moneda').hide();
        jQuery('.overlay_multiusos2').fadeOut();
    })

    jQuery('.telefono').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    jQuery('.btn_distribuidores').click(function(){
        jQuery('.overlay_multiusos2').fadeIn();
        jQuery('.login_distribuidores').show();
    })

    jQuery('.close_login').click(function(){
        jQuery('.overlay_multiusos2').fadeOut();
        jQuery('.login_distribuidores').hide();
    })

    jQuery('.nombre').keyup(function(){
        var nombre_valor=jQuery(this).val();
        if(nombre_valor.length<=30) {
            jQuery('.nombre_pulseradiseno').text(nombre_valor);
            return true;
        }
        else{
            return false;
        }

    })

    jQuery('.tipo_letra').change(function(){
        var tipo_letra=jQuery(this).val();
        jQuery('.nombre_pulseradiseno').css('font-family',''+tipo_letra+'');
    })

    jQuery('.contenido_tablapedido').on('click','.icon_view',function(){
        jQuery('.overlay_multiusos').fadeIn();
        var imgpulsera=jQuery(this).data('imgpulsera');
        jQuery('.contenido_pulsera').show();
        jQuery('.view_imgpulsera').html('<img src="'+imgpulsera+'">');
    })

    jQuery('.close_viewpulsera').click(function(){
        jQuery('.contenido_pulsera').hide();
        jQuery('.overlay_multiusos').fadeOut();
    })

    jQuery('.close_sesion').click(function(){
        localStorage.removeItem('datos_distribuidor');
        if(getUrlParameter('lang')=='en'){
            mussalert('Éxito','It has been closed session','exito');
        }else{
            mussalert('Éxito','Se ha cerrado sesion','exito');
        }

    })


    jQuery('.btnlogin').click(function(){

        var email_envio=jQuery('.email_login').val();
        var password=jQuery('.password').val();

        jQuery.ajax({
            url : '/wp-admin/admin-ajax.php',
            timeout: 8000,
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'login',
                email: email_envio,
                password: password
            },
            beforeSend: function(){
                jQuery('.overlay_loader').show();
            },
            success: function (data) {
                jQuery('.overlay_loader').hide();
                if(data.bandera==1) {
                    datos_distribuidores = {
                        'id': data.ID,
                        'nombre': data.nombre,
                        'apellido': data.apellido,
                        'contrasena': data.contrasena,
                        'country': data.country,
                        'state': data.state,
                        'ciudad': data.ciudad,
                        'direccion': data.direccion,
                        'cp': data.cp,
                        'email': data.email,
                        'telefono': data.telefono
                    };
                    localStorage.setItem('datos_distribuidor', JSON.stringify(datos_distribuidores));
                    if(getUrlParameter('lang')=='en'){
                        mussalert('Success', 'Login completed successfully', 'exito');
                    }else{
                        mussalert('Éxito', 'Inicio de sesión completado correctamente', 'exito');
                    }
                }
                else{
                    if(getUrlParameter('lang')=='en'){
                        mussalert('Error', 'An error when validating data has occurred, check your email', 'error');
                    }else{
                        mussalert('Error', 'Ha ocurrido un error al momento de validar sus datos, verifique su correo electrónico ', 'error');
                    }

                }
            },
            error: function (err) {
                jQuery('.overlay_loader').hide();
                if(getUrlParameter('lang')=='en'){
                    mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                }else{
                    mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                }

            }
        })
    })

    jQuery('.btnupdateinfo').click(function(){

        var datos_distribuidores=JSON.parse(localStorage.getItem('datos_distribuidor'));
        jQuery('.nombre_distribuidor').val(datos_distribuidores.nombre);
        jQuery('.apellido_distribuidor').val(datos_distribuidores.apellido);
        jQuery('.telefono_distribuidor').val(datos_distribuidores.telefono);
        jQuery('#country_distribuidor').val(datos_distribuidores.country);
        print_state('state_distribuidor',document.getElementById("country_distribuidor").selectedIndex);
        jQuery('#state_distribuidor').val(datos_distribuidores.state);
        jQuery('.ciudad_distribuidor').val(datos_distribuidores.ciudad);
        jQuery('.email_distribuidor').val(datos_distribuidores.email);
        jQuery('.direccion_distribuidor').val(datos_distribuidores.direccion);
        jQuery('.cp_distribuidor').val(datos_distribuidores.cp);
        jQuery('.modalupdate').show();
        jQuery('.overlay_multiusos2').fadeIn();
    })

    jQuery('.btncancelardistribuidor').click(function(){
        jQuery('.modalupdate').hide();
        jQuery('.modalupdate').find('input[type=text]').val('');
        jQuery('.modalupdate').find('select').val('');
        jQuery('.modalupdate').find('textarea').val('');
        jQuery('.overlay_multiusos2').fadeOut();
    })

    jQuery('.btnupdatedistribuidor').click(function(){

        var datos_distribuidores=JSON.parse(localStorage.getItem('datos_distribuidor'));
        var nombre_distribuidor=jQuery('.nombre_distribuidor').val();
        var apellido_distribuidor=jQuery('.apellido_distribuidor').val();
        var telefono_distribuidor=jQuery('.telefono_distribuidor').val();
        var pais_distribuidor=jQuery('#country_distribuidor').val();
        var estado_distribuidor=jQuery('#state_distribuidor').val();
        var ciudad_distribuidor=jQuery('.ciudad_distribuidor').val();
        var email_distribuidor=jQuery('.email_distribuidor').val();
        var direccion_distribuidor=jQuery('.direccion_distribuidor').val();
        var codigo_postal=jQuery('.cp_distribuidor').val();

        jQuery.ajax({
            url : '/wp-admin/admin-ajax.php',
            timeout: 8000,
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'updatecliente',
                id: datos_distribuidores.id,
                nombre: nombre_distribuidor,
                apellido: apellido_distribuidor,
                country: pais_distribuidor,
                state: estado_distribuidor,
                ciudad: ciudad_distribuidor,
                direccion: direccion_distribuidor,
                codigo_postal: codigo_postal,
                email: email_distribuidor,
                telefono: telefono_distribuidor

            },
            beforeSend: function(){
                jQuery('.overlay_loader').show();
            },
            success: function (data) {
                jQuery('.overlay_loader').hide();
                datos_distribuidores.nombre=nombre_distribuidor;
                datos_distribuidores.apellido=apellido_distribuidor;
                datos_distribuidores.country=pais_distribuidor;
                datos_distribuidores.state=estado_distribuidor;
                datos_distribuidores.ciudad=ciudad_distribuidor;
                datos_distribuidores.direccion=direccion_distribuidor
                datos_distribuidores.email=email_distribuidor;
                datos_distribuidores.telefono=telefono_distribuidor;
                localStorage.setItem('datos_distribuidor',JSON.stringify(datos_distribuidores));
                if(getUrlParameter('lang')=='en'){
                    jQuery('.bienvenido').find('p').html('Welcome: '+datos_distribuidores.nombre);
                    mussalert('Success', 'Update completed', 'exito');
                }else{
                    jQuery('.bienvenido').find('p').html('Bienvenido: '+datos_distribuidores.nombre);
                    mussalert('Éxito','Datos actualizados correctamente','exito');
                }
            },
            error: function (err) {
                jQuery('.overlay_loader').hide();
                if(getUrlParameter('lang')=='en'){
                    mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                }else{
                    mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                }
            }
        })
    })

    jQuery('.numbers').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    jQuery('.btn_moreproduct').click(function(){
        jQuery('.modal_personaliza').hide();
        jQuery('.mensaje_pedido').css('opacity','0');
        jQuery('.mensaje_pedido').css('visibility','hidden');
        jQuery('.overlay_multiusos').fadeOut();
        jQuery('.boton_talla').removeClass('select');
        jQuery('.boton_talla').removeClass('disable');
    })

    if(localStorage.getItem('eternita_pedido')) {
        var contador_carrito = JSON.parse(localStorage.getItem('eternita_pedido'));
        var list='';
        var precio='';
        jQuery('.contador_pedidos').text(contador_carrito.length);

        for(var z=0; z < contador_carrito.length; z++){

            if(contador_carrito[z].disenopulsera.tallas.talla_s){
                var talla_s='<span>S</span>';
                var cantidad_s=contador_carrito[z].disenopulsera.tallas.talla_s;
            }
            else{
                var talla_s='<span></span>';
                var cantidad_s=0;
            }

            if(contador_carrito[z].disenopulsera.tallas.talla_m){
                var talla_m='<span>M</span>';
                var cantidad_m=contador_carrito[z].disenopulsera.tallas.talla_m;
            }
            else{
                var talla_m='<span></span>';
                var cantidad_m=0;
            }

            if(contador_carrito[z].disenopulsera.tallas.talla_l){
                var talla_l='<span>L</span>';
                var cantidad_l=contador_carrito[z].disenopulsera.tallas.talla_l;
            }
            else{
                var talla_l='<span></span>';
                var cantidad_l=0;
            }

            if(contador_carrito[z].disenopulsera.tallas.talla_p){
                var talla_p='<span>P</span>';
                var cantidad_p=contador_carrito[z].disenopulsera.tallas.talla_p;
            }
            else{
                var talla_p='<span></span>';
                var cantidad_p=0;
            }

            precio=parseInt(contador_carrito[z].disenopulsera.precio_talla);

            list +='<li><p>Nombre: '+contador_carrito[z].datos_generales.nombre+'</p><p>Talla: '+talla_s+talla_m+talla_l+talla_p+'</p><p>Precio: '+precio.formatMoney()+'</p></li>';
            precio='';

        }

        jQuery('.list_pedidos').find('ul').html(list);
    }

    if(localStorage.getItem('datos_distribuidor')){
        jQuery('.btn_distribuidores').hide();
        var datos_dist=JSON.parse(localStorage.getItem('datos_distribuidor'));
        jQuery('.btn_distribuidores').hide();
        jQuery('.bienvenido').show();
        if(getUrlParameter('lang')=='en'){
            jQuery('.bienvenido').find('p').html('Welcome: '+datos_dist.nombre);
        }else{
            jQuery('.bienvenido').find('p').html('Bienvenido: '+datos_dist.nombre);
        }

        if(bodyclasspanel){
            var list_pedidos='';
            if(getUrlParameter('lang')=='en'){
                jQuery('.datos_distribuidor').html('<p><span>Name: </span><span>'+datos_dist.nombre+' '+datos_dist.apellido+'</span></p><p><span>E-mail: </span><span>'+datos_dist.email+'</span></p><p><span>Telephone: </span><span>'+datos_dist.telefono+'</span></p><p><span>Country: </span><span>'+datos_dist.country+'</span></p><p><span>State: </span><span>'+datos_dist.state+'</span></p><p><span>City: </span><span>'+datos_dist.ciudad+'</span></p><p><span>Address: </span><span>'+datos_dist.direccion+'</span></p><p><span>Postal ZIP: </span><span>'+datos_dist.cp+'</span></p>');
            }else{
                jQuery('.datos_distribuidor').html('<p><span>Nombre completo: </span><span>'+datos_dist.nombre+' '+datos_dist.apellido+'</span></p><p><span>Correo electronico: </span><span>'+datos_dist.email+'</span></p><p><span>Teléfono: </span><span>'+datos_dist.telefono+'</span></p><p><span>País: </span><span>'+datos_dist.country+'</span></p><p><span>Estado: </span><span>'+datos_dist.state+'</span></p><p><span>Ciudad: </span><span>'+datos_dist.ciudad+'</span></p><p><span>Dirección: </span><span>'+datos_dist.direccion+'</span></p><p><span>Codigo postal: </span><span>'+datos_dist.cp+'</span></p>');
            }
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'getpedidosxdistribuidor',
                    distribuidor: datos_dist.nombre
                },
                beforeSend: function () {
                    jQuery('.overlay_loader').show();
                },
                success: function (data) {
                    jQuery('.overlay_loader').hide();

                    if(getUrlParameter('lang')=='en'){
                        var titulos1='Date';
                    }else{
                        var titulos1='Fecha';
                    }

                    for(var m=0; m < data.length; m++){

                        switch(data[m].estatus) {
                            case 'Nuevo':
                                if(getUrlParameter('lang')=='en'){
                                    var namestatus='New';
                                }else{
                                    var namestatus='Nuevo';
                                }
                                var color='#FF4069';
                                break;
                            case 'En proceso':
                                if(getUrlParameter('lang')=='en'){
                                    var namestatus='In process';
                                }else{
                                    var namestatus='En proceso';
                                }
                                var color='#FFD625';
                                break;
                            case 'Esperando':
                                if(getUrlParameter('lang')=='en'){
                                    var namestatus='Waiting';
                                }else{
                                    var namestatus='Esperando';
                                }
                                var color='#FD8700';
                                break;
                            case 'Enviado':
                                if(getUrlParameter('lang')=='en'){
                                    var namestatus='Dispatched';
                                }else{
                                    var namestatus='Enviado';
                                }
                                var color='#00A99D';
                                break;
                            case 'Enviado y entregado':
                                if(getUrlParameter('lang')=='en'){
                                    var namestatus='Shipped and delivered';
                                }else{
                                    var namestatus='Enviado y entregado';
                                }
                                var color='#8CC63F';
                                break;
                            case 'Cancelado':
                                if(getUrlParameter('lang')=='en'){
                                    var namestatus='Canceled';
                                }else{
                                    var namestatus='Cancelado';
                                }
                                var color='#D40000';
                                break;
                        }

                        list_pedidos +='<li><a href="'+location.origin+'/resumen-pedido/?id='+data[m].ID+'"><div class="img"><img src="http://eternitta.mx/wp-content/themes/eternitta/img/home.jpg"></div><div class="estatus"><label style="background-color: '+color+'"></label><span>'+namestatus+'</span></div><div class="datos"><span>'+titulos1+': </span><spa>'+data[m].fecha+'</spa></div><div class="datos"><span>Total: </span><spa>'+parseInt(data[m].total).formatMoney()+'</spa></div></a></li>';
                    }

                    jQuery('.listado_pedidos').html(list_pedidos);

                    jQuery('.filtro').change(function(){
                        list_pedidos='';
                        jQuery('.listado_pedidos').empty();
                        var orden=jQuery(this).val();
                        var ordenados=_.map(_.sortByOrder(data, [''+orden+''], ['asc']), _.values);
                        if(getUrlParameter('lang')=='en'){
                            var titulos1='Date';
                        }else{
                            var titulos1='Fecha';
                        }
                        for(var m=0; m < ordenados.length; m++){
                            switch(ordenados[m][3]) {
                                case 'Nuevo':
                                    if(getUrlParameter('lang')=='en'){
                                        var namestatus='New';
                                    }else{
                                        var namestatus='Nuevo';
                                    }
                                    var color='#FF4069';
                                    break;
                                case 'En proceso':
                                    if(getUrlParameter('lang')=='en'){
                                        var namestatus='In process';
                                    }else{
                                        var namestatus='En proceso';
                                    }
                                    var color='#FFD625';
                                    break;
                                case 'Esperando':
                                    if(getUrlParameter('lang')=='en'){
                                        var namestatus='Waiting';
                                    }else{
                                        var namestatus='Esperando';
                                    }
                                    var color='#FD8700';
                                    break;
                                case 'Enviado':
                                    if(getUrlParameter('lang')=='en'){
                                        var namestatus='Dispatched';
                                    }else{
                                        var namestatus='Enviado';
                                    }
                                    var color='#00A99D';
                                    break;
                                case 'Enviado y entregado':
                                    if(getUrlParameter('lang')=='en'){
                                        var namestatus='Shipped and delivered';
                                    }else{
                                        var namestatus='Enviado y entregado';
                                    }
                                    var color='#8CC63F';
                                    break;
                                case 'Cancelado':
                                    if(getUrlParameter('lang')=='en'){
                                        var namestatus='Canceled';
                                    }else{
                                        var namestatus='Cancelado';
                                    }
                                    var color='#D40000';
                                    break;
                            }

                            list_pedidos +='<li><a href="'+location.origin+'/resumen-pedido/?id='+data[m][0]+'"><div class="img"><img src="http://eternitta.mx/wp-content/themes/eternitta/img/home.jpg"></div><div class="estatus"><label style="background-color: '+color+'"></label><span>'+namestatus+'</span></div><div class="datos"><span>'+titulos1+': </span><spa>'+ordenados[m][1]+'</spa></div><div class="datos"><span>Total: </span><spa>'+parseInt(ordenados[m][2]).formatMoney()+'</spa></div></a></li>';
                        }
                        jQuery('.listado_pedidos').html(list_pedidos);
                    })
                },
                error: function (err) {
                    jQuery('.overlay_loader').hide();
                    if(getUrlParameter('lang')=='en'){
                        mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                    }else{
                        mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                    }

                }
            })
        }
    }
    else{
        jQuery('.bienvenido').hide();
        jQuery('.btn_distribuidores').show();
    }


    jQuery('.icon_cart').click(function(){
        if(click_carrito==0) {
            jQuery('.list_pedidos').slideDown();
            click_carrito=1;
        }
        else{
            jQuery('.list_pedidos').slideUp();
            click_carrito=0;
        }
    })

    localStorage.removeItem('eternita_tallas');
    localStorage.removeItem('eternita_tallam');
    localStorage.removeItem('eternita_tallal');
    localStorage.removeItem('eternita_tallap');

    if(window.location.hash) {
        var x = location.hash;
            var target = jQuery(x);
            if (target.length) {
                position=target.offset().top;
                jQuery('html,body').animate({
                    scrollTop: position
                }, 1000);
                return false;
            }
    } else {
       
    }

    jQuery('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        responsive:{
            0:{
                items:1,
                nav:true,
                navText:['Anterior','Siguiente']
            },
            600:{
                items:3,
                nav:true,
                navText:['Anterior','Siguiente']
            },
            1000:{
                items:4,
                nav:true,
                navText:['<span><img src="'+location.origin+'/wp-content/themes/eternitta/img/arrow_left.png"></span>','<span><img src="'+location.origin+'/wp-content/themes/eternitta/img/arrow_right.png"></span>']
            }
        }
    })


        jQuery( ".fecha_nacimiento" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "-70:+0"
        });

    jQuery('.menu_mobil').click(function(){
        jQuery('.main-navigation').fadeIn();
        jQuery('.menu').addClass('mostar');
    })

    jQuery('.close_menu').click(function(){
        jQuery('.main-navigation').fadeOut();
        jQuery('.menu').removeClass('mostar');
    })

    jQuery('.close_pago').click(function(){
        jQuery('.modal_formaspago').hide();
        jQuery('.overlay_multiusos').fadeOut();
    })


    jQuery('.btn_cancelar').click(function(){
        /*jQuery('.overlay_multiusos').fadeOut();
        jQuery('.edit_pedido').css('opacity','1');
        jQuery('.edit_pedido').css('visibility','hidden');*/
        var selector= jQuery('.mensaje_confirm_pulsera');
        selector.css('opacity','1');
        selector.css('visibility','visible');
        if(getUrlParameter('lang')=='en'){
            selector.find('p').html('Exit');


        }else{
            selector.find('p').html('Estas apunto de salir');
        }

        jQuery('.overlay_multiusos').fadeIn();
    })

    jQuery('.btn_cancelar_edit').click(function(){
        /*jQuery('.overlay_multiusos').fadeOut();
         jQuery('.edit_pedido').css('opacity','1');
         jQuery('.edit_pedido').css('visibility','hidden');*/
        var selector= jQuery('.edit_pedido');
        selector.css('opacity','0');
        selector.css('visibility','hidden');
        jQuery('.overlay_multiusos').fadeOut();
    })

    var tallas=localStorage.getItem('eternita_tallas');
    var tallam=localStorage.getItem('eternita_tallam');
    var tallal=localStorage.getItem('eternita_tallal');
    var tallap=localStorage.getItem('eternita_tallap');


    if(tallas){
        if(tallas > 0) {
            jQuery('#tallas').addClass('select');
            jQuery('#tallam').addClass('disable');
            jQuery('#tallal').addClass('disable');
            jQuery('#tallap').addClass('disable');
        }
    }
    if(tallam) {
        if (tallam > 0) {
            jQuery('#tallam').addClass('select');
            jQuery('#tallas').addClass('disable');
            jQuery('#tallal').addClass('disable');
            jQuery('#tallap').addClass('disable');
        }
    }

    if(tallal) {
        if (tallal > 0) {
            jQuery('#tallal').addClass('select');
            jQuery('#tallas').addClass('disable');
            jQuery('#tallam').addClass('disable');
            jQuery('#tallap').addClass('disable');
        }
    }

    if(tallap) {
        if (tallap > 0) {
            jQuery('#tallap').addClass('select');
            jQuery('#tallas').addClass('disable');
            jQuery('#tallam').addClass('disable');
            jQuery('#tallal').addClass('disable');
        }
    }

    var listado_tallas=jQuery('.talla');
        for (var i = 0; i < listado_tallas.length; i++) {
                var talla_selec = jQuery(listado_tallas[i]).data('talla');
            if(localStorage.getItem('eternita_talla' + talla_selec)) {
                var cantidad_talla = localStorage.getItem('eternita_talla' + talla_selec);
                jQuery(listado_tallas[i]).find('.cantidad_pedido').text(cantidad_talla);
            }

        }

    jQuery('.btn_cerrar').click(function(){
        if(jQuery(this).parents('.mensaje').hasClass('exito')){
            location.reload();
        }
        else {
            mussalert_close();
        }
        //location.href = redir + '/tienda';
    });

    /*jQuery('.btn_cerrar_pedido').click(function(){

        jQuery('.mensaje_pedido').css('opacity',0);
        jQuery('.mensaje_pedido').css('visibility','hidden');
        jQuery('.overlay_multiusos').fadeOut();
        //location.href = redir + '/tienda';
    });*/

    jQuery('.btn_agregar').click(function(){

        var nombre= jQuery('.nombre').val();
        var apellido = jQuery('.apellido').val();
        var contrasena = jQuery('.contrasena').val();
        var country = jQuery('#country').val();
        var state = jQuery('#state').val();
        var ciudad = jQuery('.ciudad').val();
        var direccion = jQuery('.direccion').val();
        var codigo_postal= jQuery('.codigo_postal').val();
        var email = jQuery('.email').val();
        var telefono = jQuery('.telefono').val();
        var datos_distribuidores='';

        if(nombre !='', apellido!='' , contrasena!='' , country!='' , state !='' , ciudad !='' , direccion !='', codigo_postal !='' , email !='' , telefono !=''){

            jQuery.ajax({
                url : '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'addcliente',
                    nombre: nombre,
                    apellido: apellido,
                    contrasena: contrasena,
                    country: country,
                    state: state,
                    ciudad: ciudad,
                    direccion: direccion,
                    codigo_postal: codigo_postal,
                    email: email,
                    telefono: telefono
                },
                beforeSend: function(){
                    jQuery('.overlay_loader').show();
                },
                success: function (data) {
                    console.log(data);
                    jQuery('.overlay_loader').hide();
                    datos_distribuidores={'nombre' : data.nombre, 'apellido': data.apellido, 'contrasena': data.contrasena, 'country': data.country, 'state': data.state, 'ciudad': data.ciudad, 'direccion':data.direccion, 'email': data.email, 'telefono':data.telefono };
                    console.log(datos_distribuidores);
                    localStorage.setItem('datos_distribuidor',JSON.stringify(datos_distribuidores));

                    if(getUrlParameter('lang')=='en'){
                        mussalert('success', 'Registration successfully completed', 'exito');
                    }else{
                        mussalert('Éxito','Registro completado correctamente','exito');
                    }
                },
                error: function (err) {
                    jQuery('.overlay_loader').hide();
                    if(getUrlParameter('lang')=='en'){
                        mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                    }else{
                        mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                    }
                }
            })

        }
        else{
            alert('no llenaste nada');
        }

    })


    jQuery('.contenido_tallas').on('click','.talla',function(){
        jQuery('.overlay_multiusos2').fadeIn();
        jQuery('.modal_personaliza').show();
        var talla= jQuery(this).data('talla');
        var precio_talla=jQuery(this).data('precio');
        jQuery('.precio_talla').val(precio_talla);
        jQuery('.disabled_seccion').fadeOut();

        switch(talla) {
            case 's':
                var cantidad_almacenada=localStorage.getItem('eternita_tallas');
                if(!cantidad_almacenada){
                    localStorage.setItem('eternita_tallas', 1);
                }
                break;
            case 'm':
                var cantidad_almacenada=localStorage.getItem('eternita_tallam');
                if(!cantidad_almacenada){
                    localStorage.setItem('eternita_tallam', 1);
                }
                break;

            case 'l':
                var cantidad_almacenada=localStorage.getItem('eternita_tallal');
                if(!cantidad_almacenada){
                    localStorage.setItem('eternita_tallal', 1);
                }
                break;

            case 'p':
                var cantidad_almacenada=localStorage.getItem('eternita_tallap');
                if(!cantidad_almacenada){
                    localStorage.setItem('eternita_tallap', 1);
                }
                break;

        }
        var cantidad_anterior=jQuery(this).find('.cantidad_pedido').text();
        jQuery(this).find('.cantidad_pedido').text(1);

        var tallas=localStorage.getItem('eternita_tallas');
        var tallam=localStorage.getItem('eternita_tallam');
        var tallal=localStorage.getItem('eternita_tallal');
        var tallap=localStorage.getItem('eternita_tallap');

        if(tallas){
            if(tallas > 0) {
                jQuery('#tallas').addClass('select');
                jQuery('#tallam').addClass('disable');
                jQuery('#tallal').addClass('disable');
                jQuery('#tallap').addClass('disable');
            }
        }
        if(tallam) {
            if (tallam > 0) {
                jQuery('#tallam').addClass('select');
                jQuery('#tallas').addClass('disable');
                jQuery('#tallal').addClass('disable');
                jQuery('#tallap').addClass('disable');
            }
        }

        if(tallal) {
            if (tallal > 0) {
                jQuery('#tallal').addClass('select');
                jQuery('#tallas').addClass('disable');
                jQuery('#tallam').addClass('disable');
                jQuery('#tallap').addClass('disable');
            }
        }

        if(tallap) {
            if (tallap > 0) {
                jQuery('#tallap').addClass('select');
                jQuery('#tallas').addClass('disable');
                jQuery('#tallam').addClass('disable');
                jQuery('#tallal').addClass('disable');
            }
        }

        jQuery('.owl-carousel_pedido').owlCarousel({
            loop:true,
            margin:10,
            autoplay:true,
            autoplayTimeout:2000,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1,
                    nav:false,
                    navText:['Anterior','Siguiente']
                },
                600:{
                    items:1,
                    nav:false,
                    navText:['Anterior','Siguiente']
                },
                1000:{
                    items:1,
                    nav:false,
                    navText:['<span><img src="'+location.origin+'/wp-content/themes/eternitta/img/arrow_left.png"></span>','<span><img src="'+location.origin+'/wp-content/themes/eternitta/img/arrow_right.png"></span>']
                }
            }
        })

    })

    jQuery('.rest_cantidad').click(function(){
        var talla= jQuery(this).data('talla');

        switch(talla) {
            case 's':
                var cantidad_almacenada=localStorage.getItem('eternita_tallas');
                if(cantidad_almacenada){
                    if(cantidad_almacenada>0) {
                        localStorage.setItem('eternita_tallas', parseInt(cantidad_almacenada) - 1);
                    }
                    else{
                        localStorage.setItem('eternita_tallas', parseInt(cantidad_almacenada));
                    }
                }else{
                    localStorage.setItem('eternita_tallas', 1);
                }

                break;
            case 'm':
                var cantidad_almacenada=localStorage.getItem('eternita_tallam');
                if(cantidad_almacenada){
                    if(cantidad_almacenada>0) {
                        localStorage.setItem('eternita_tallam', parseInt(cantidad_almacenada) - 1);
                    }
                    else{
                        localStorage.setItem('eternita_tallam', parseInt(cantidad_almacenada));
                    }
                }
                else{
                    localStorage.setItem('eternita_tallam', 1);
                }

                break;

            case 'l':
                var cantidad_almacenada=localStorage.getItem('eternita_tallal');
                if(cantidad_almacenada){
                    if(cantidad_almacenada>0) {
                        localStorage.setItem('eternita_tallal', parseInt(cantidad_almacenada) - 1);
                    }
                    else{
                        localStorage.setItem('eternita_tallal', parseInt(cantidad_almacenada));
                    }
                }
                else{
                    localStorage.setItem('eternita_tallal', 1);
                }

                break;

            case 'p':
                var cantidad_almacenada=localStorage.getItem('eternita_tallap');
                if(cantidad_almacenada){
                    if(cantidad_almacenada>0) {
                        localStorage.setItem('eternita_tallap', parseInt(cantidad_almacenada) - 1);
                    }
                    else{
                        localStorage.setItem('eternita_tallap', parseInt(cantidad_almacenada));
                    }
                }
                else{
                    localStorage.setItem('eternita_tallap', 1);
                }

                break;

        }

        var parent_selectro=jQuery(this).parent();
        var cantidad_anterior=jQuery(parent_selectro[0]).find('.talla').find('.cantidad_pedido').text()
        if(cantidad_anterior>0) {
            jQuery(parent_selectro[0]).find('.talla').find('.cantidad_pedido').text(parseInt(cantidad_anterior) - 1);
        }

        var tallasrestcheck=localStorage.getItem('eternita_tallas');
        var tallamrestcheck=localStorage.getItem('eternita_tallam');
        var tallalrestcheck=localStorage.getItem('eternita_tallal');
        var tallaprestcheck=localStorage.getItem('eternita_tallap');

        if(tallasrestcheck || tallamrestcheck || tallalrestcheck || tallaprestcheck){
            if(tallasrestcheck > 0 || tallamrestcheck > 0 || tallalrestcheck > 0 || tallaprestcheck > 0) {
                jQuery('.disabled_seccion').fadeOut();
            }
            else{
                jQuery('.disabled_seccion').fadeIn();
            }
        }
    })


    jQuery('.more_cantidad').click(function(){
        var talla= jQuery(this).data('talla');

        switch(talla) {
            case 's':
                var cantidad_almacenada=localStorage.getItem('eternita_tallas');
                if(cantidad_almacenada){
                    if(cantidad_almacenada<99) {
                        localStorage.setItem('eternita_tallas', parseInt(cantidad_almacenada) + 1);
                    }
                    else{
                        localStorage.setItem('eternita_tallas', parseInt(cantidad_almacenada));
                    }
                }else{
                    localStorage.setItem('eternita_tallas', 1);
                }

                break;
            case 'm':
                var cantidad_almacenada=localStorage.getItem('eternita_tallam');
                if(cantidad_almacenada){
                    if(cantidad_almacenada<99) {
                        localStorage.setItem('eternita_tallam', parseInt(cantidad_almacenada) + 1);
                    }else{
                        localStorage.setItem('eternita_tallam', parseInt(cantidad_almacenada));
                    }
                }
                else{
                    localStorage.setItem('eternita_tallam', 1);
                }

                break;

            case 'l':
                var cantidad_almacenada=localStorage.getItem('eternita_tallal');
                if(cantidad_almacenada){
                    if(cantidad_almacenada<99) {
                        localStorage.setItem('eternita_tallal', parseInt(cantidad_almacenada) + 1);
                    }
                    else{
                        localStorage.setItem('eternita_tallal', parseInt(cantidad_almacenada));
                    }
                }
                else{
                    localStorage.setItem('eternita_tallal', 1);
                }

                break;

            case 'p':
                var cantidad_almacenada=localStorage.getItem('eternita_tallap');
                if(cantidad_almacenada){
                    if(cantidad_almacenada<99) {
                        localStorage.setItem('eternita_tallap', parseInt(cantidad_almacenada) + 1);
                    }else{
                        localStorage.setItem('eternita_tallap', parseInt(cantidad_almacenada));
                    }
                }
                else{
                    localStorage.setItem('eternita_tallap', 1);
                }

                break;

        }

        var parent_selectro=jQuery(this).parent();
        var cantidad_anterior=jQuery(parent_selectro[0]).find('.talla').find('.cantidad_pedido').text()
        if(cantidad_anterior<99) {
            jQuery(parent_selectro[0]).find('.talla').find('.cantidad_pedido').text(parseInt(cantidad_anterior) + 1);
        }

        var tallasmorecheck=localStorage.getItem('eternita_tallas');
        var tallammorecheck=localStorage.getItem('eternita_tallam');
        var tallalmorecheck=localStorage.getItem('eternita_tallal');
        var tallapmorecheck=localStorage.getItem('eternita_tallap');

        if(tallasmorecheck || tallammorecheck || tallalmorecheck || tallapmorecheck){
            if(tallasmorecheck > 0 || tallammorecheck > 0 || tallalmorecheck > 0 || tallapmorecheck > 0) {
                jQuery('.disabled_seccion').fadeOut();
            }
            else{
                jQuery('.disabled_seccion').fadeIn();
            }
        }

    })



    /*jQuery('.cantidad_pedido').change(function(){

        var talla= jQuery(this).data('talla');
        var cantidad= jQuery(this).val();

        jQuery('.disabled_seccion').fadeOut();

        switch(talla) {
            case 's':
                var cantidad_almacenada=localStorage.getItem('eternita_tallas');

                    localStorage.setItem('eternita_tallas', parseInt(cantidad));

                break;
            case 'm':
                var cantidad_almacenada=localStorage.getItem('eternita_tallam');

                    localStorage.setItem('eternita_tallam', parseInt(cantidad));

                break;

            case 'l':
                var cantidad_almacenada=localStorage.getItem('eternita_tallal');

                    localStorage.setItem('eternita_tallal', parseInt(cantidad));

                break;

            case 'p':
                var cantidad_almacenada=localStorage.getItem('eternita_tallap');
                    localStorage.setItem('eternita_tallap', parseInt(cantidad));

                break;

        }
        //var cantidad_anterior=jQuery(this).find('.cantidad_pedido').val();
        //jQuery(this).find('.cantidad_pedido').val(parseInt(cantidad_anterior)+1);

    })*/

    jQuery('.btn_listo [href*=#]:not([href=#])').click(function(){

        var tallas2=localStorage.getItem('eternita_tallas');
        var tallam2=localStorage.getItem('eternita_tallam');
        var tallal2=localStorage.getItem('eternita_tallal');
        var tallap2=localStorage.getItem('eternita_tallap');

        if(tallas2 || tallam2 || tallal2 || tallap2){
            jQuery('.disabled_seccion').fadeOut();
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    position=target.offset().top-35;
                    jQuery('html,body').animate({
                        scrollTop: position
                    }, 1000);
                    return false;
                }
            }
        }
        else
        {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    position=target.offset().top-35;
                    jQuery('html,body').animate({
                        scrollTop: position
                    }, 1000);
                    return false;
                }
            }

        }
    })
    jQuery('.comprar_btn').find('a').click(function(){
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    position=target.offset().top-35;
                    jQuery('html,body').animate({
                        scrollTop: position
                    }, 1000);
                    return false;
                }
            }
    })

    jQuery('.btn_pedido').click(function(){
        jQuery('.overlay_loader').show();
        jQuery('.nombre_pulseradiseno').css('border','none');
        html2canvas(jQuery('.container_pulsera'), {
            onrendered: function(canvas) {
                var img=convertCanvasToImage(canvas);
                localStorage.setItem('img_pulsera',img);
            }
        });
        setTimeout(function() {
            insert_pedido();
        }, 5000);
    })

    var pedido_storage=localStorage.getItem('eternita_pedido');

    if(pedido_storage) {
        var contenido_tablapedido='';
        var pedido_desglose = JSON.parse(pedido_storage);
        var total_global=0;
        var subtotal=0;
        var total=0;
        var costo_envio=0;

        for(var i =0; i < pedido_desglose.length; i++){

            if(pedido_desglose[i].disenopulsera.tallas.talla_s){
                var talla_s='<span>Talla s</span>';
                var cantidad_s=pedido_desglose[i].disenopulsera.tallas.talla_s;
            }
            else{
                var talla_s='<span></span>';
                var cantidad_s=0;
            }

            if(pedido_desglose[i].disenopulsera.tallas.talla_m){
                var talla_m='<span>Talla m</span>';
                var cantidad_m=pedido_desglose[i].disenopulsera.tallas.talla_m;
            }
            else{
                var talla_m='<span></span>';
                var cantidad_m=0;
            }

            if(pedido_desglose[i].disenopulsera.tallas.talla_l){
                var talla_l='<span>Talla l</span>';
                var cantidad_l=pedido_desglose[i].disenopulsera.tallas.talla_l;
            }
            else{
                var talla_l='<span></span>';
                var cantidad_l=0;
            }

            if(pedido_desglose[i].disenopulsera.tallas.talla_p){
                var talla_p='<span>Talla p</span>';
                var cantidad_p=pedido_desglose[i].disenopulsera.tallas.talla_p;
            }
            else{
                var talla_p='<span></span>';
                var cantidad_p=0;
            }

            subtotal=parseInt(cantidad_s)+parseInt(cantidad_m)+parseInt(cantidad_l)+parseInt(cantidad_p);
            total=parseInt(subtotal)*parseInt(pedido_desglose[i].disenopulsera.precio_talla);
            total_global += parseInt(total)
            console.log(total_global);

            contenido_tablapedido += '<li><div class="overlay_pulsera"><img data-imgpulsera="'+pedido_desglose[i].disenopulsera.imgpulsera+'" class="icon_view" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2M0E4NjFGOTdCMzkxMUU1QjE1NUE4OTdCQ0Y2Nzg3MiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2M0E4NjFGQTdCMzkxMUU1QjE1NUE4OTdCQ0Y2Nzg3MiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjYzQTg2MUY3N0IzOTExRTVCMTU1QTg5N0JDRjY3ODcyIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjYzQTg2MUY4N0IzOTExRTVCMTU1QTg5N0JDRjY3ODcyIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+V42W6wAAAgRJREFUeNrMmO1tgzAQhknVATwCG8QbxN0gI2QEukE6QdsJ6AbpBoQJIBOQDUInoLbkSinhte/M5eOk+0Ni89jcx2svhmHIHtmeE8Zo68b60no++u1o/WB9b70VIXQ7SPSN9W6gm/tvYV0x3nHhlD+tmWBjO/k5rgJYDnK2S9nNBUgSZb3y8SZpLi5frPfUAQiwIcC5RKhHz1zirEUhJ7b1PRJP28inUj6hToF5ytQYNIIxpPwYZOsUwG7uihmJ1lEWPK5zaOeymY4gNxzABsScEgBU4Ot0sbFPPldykLWfnJIQMDfH28TzPFYt/gANmPRDsAZ+gcUaCuAS1LleuFB/TzxbUQCntvlwBfVUg64VBUSdQtqOIA6TAG9lyYDqRoBtKqC+AowG1SIKWHOzK9FWxLi8AGxBfcoF4RSoeTUFEGXsRhCwAHG9p+pBJI20QC/OgT6sOGIBacFmpmBQQIiw1UzmVyQJGROtmguoI+dcw4DTgZ07l3Oae+wsIpOWkUk187gahESnupKQwb0vT85/fI3LE0tT7096LefqQ/LQnryTlPuY0z0hqTWsmvHSihmX/yA5JcP4l1B3tJzI+h0DUoWSJGbGKxPUuvaBfky983HnoddMoI1JdpaLNngPRY1LygNJfgpky70CvuXnbuYmSSYsZIsztd36W4g+dIH5MPYrwAAVel/6p7wJlgAAAABJRU5ErkJggg=="/> </div><span class="titulos_responsivos">Imagen</span><img src="'+pedido_desglose[i].disenopulsera.imgpulsera+'"></li><li><span class="titulos_responsivos">Nombre: </span>'+pedido_desglose[i].datos_generales.nombre+'</li><li class="tallas">'+talla_s+talla_m+talla_l+talla_p+'</span></li><li><span class="titulos_responsivos">Tipo de piedra: </span>'+pedido_desglose[i].disenopulsera.tipo_piedra+'</li><li><span class="titulos_responsivos">Total: </span>'+total.formatMoney()+'</li><li class="acciones"><span class="titulos_responsivos">Acciones: </span><span><div data-identificador="'+i+'" class="glyph-icon flaticon-edit45 edit"></div></span><span><div data-identificador="'+i+'" class="glyph-icon flaticon-delete30 delete"></div></span></li>';
            cantidad_s=0;
            cantidad_m=0;
            cantidad_l=0;
            cantidad_p=0;
            total=0;
            subtotal=0;
        }
        //var contenido_tablapedido= '<li>'+pedido_desglose.datos_generales.nombre+'</li><li>Tallas</li><li>'+pedido_desglose.disenopulsera.tipo_piedra+'</li><li>Total</li><li>Editar</li>'
        jQuery('.contenido_tablapedido').html(contenido_tablapedido);
        jQuery('.costo_envio').find('p').text(costo_envio.formatMoney());
        jQuery('.total_global').find('p').text(total_global.formatMoney()+' '+localStorage.getItem('currency_code'));
        jQuery('.total_global_hidden').val(total_global);
    }
    else{
        if(getUrlParameter('lang')=='en'){
            jQuery('.mensaje_pedidovacio').html('There is no order')
        }else{
            jQuery('.mensaje_pedidovacio').html('No hay ningun pedido')
        }

    }

    jQuery('.edit').click(function(){
        var identificador=jQuery(this).data('identificador');
        var pedido_desglose = JSON.parse(pedido_storage);
        var piedras='';
        var option_opiedras='';

            var telefono= '<p>Telefono: <input class="telefono_edit" type="text" name="telefono_edit" value="'+pedido_desglose[identificador].datos_generales.telefono+'"></p>';
            var fecha_nacimiento= '<p>Fecha de nacimiento: <input class="fecha_nacimiento_edit" type="text" name="fecha_nacimiento_edit" value="'+pedido_desglose[identificador].datos_generales.fecha_nacimiento+'"></p>';
            var tipo_sangre= '<p>Tipo de sangre: <input class="tipo_sangre_edit" type="text" name="tipo_sangre_edit" value="'+pedido_desglose[identificador].datos_generales.tipo_sangre+'"></p>';
            var donador_organos= '<p>Donador de organos: <select class="donador_organos"><option value="no">No</option><option value="si">Si</option></select></p>';

             if(pedido_desglose[identificador].disenopulsera.tallas.talla_s){
             var talla_s_edit='<p>Talla</p><select class="talla_pedido"><option selected="selected" value="s">S</option><option value="m">M</option><option value="l">L</option><option value="p">P</option></select>';
             }
             else{
                 var talla_s_edit='';
             }

             if(pedido_desglose[identificador].disenopulsera.tallas.talla_m){
             var talla_m_edit='<p>Talla</p><select class="talla_pedido"><option value="s">S</option><option selected="selected" value="m">M</option><option value="l">L</option><option value="p">P</option></select>';
             }
             else{
                 var talla_m_edit='';
             }


             if(pedido_desglose[identificador].disenopulsera.tallas.talla_l){
             var talla_l_edit='<p>Talla</p><select class="talla_pedido"><option value="s">S</option><option value="m">M</option><option selected="selected" value="l">L</option><option value="p">P</option></select>';
             }else{
                 var talla_l_edit='';
             }

             if(pedido_desglose[identificador].disenopulsera.tallas.talla_p){
             var talla_p_edit='<p>Talla</p><select class="talla_pedido"><option value="s">S</option><option value="m">M</option><option value="l">L</option><option selected="selected" value="p">P</option></select>';
             }else{
                 var talla_p_edit='';

             }
             var precio_talla='<p>Precio: <input disabled="disabled" class="precio_talla" type="text" name="precio_talla" value="'+pedido_desglose[identificador].disenopulsera.precio_talla+'"></p>'

        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            timeout: 8000,
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'getpiedras'
            },
            beforeSend: function () {
                jQuery('.overlay_loader').show();
            },
            success: function (data) {
                //var new_precio=JSON.parse(data);
                jQuery('.overlay_loader').hide();
                var listado_piedras=data[0];
                for(var e=0; e < listado_piedras.length; e++){
                    piedras+='<option value="'+listado_piedras[e].nombrepiedra+'">'+listado_piedras[e].nombrepiedra+'</option>';
                }
                option_opiedras='<p>Piedras: <select class="piedras">'+piedras+'</select></p>';
                var mensaje=telefono+fecha_nacimiento+tipo_sangre+donador_organos+option_opiedras+precio_talla+talla_s_edit+talla_m_edit+talla_l_edit+talla_p_edit;
                jQuery('.overlay_multiusos').fadeIn();
                jQuery('.edit_pedido').css('opacity',1);
                jQuery('.edit_pedido').css('visibility','visible');
                jQuery('.edit_pedido').find('.contenido_modal').html(mensaje)

                jQuery('.fecha_nacimiento_edit').datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0"
                });

                jQuery('.donador_organos').val(pedido_desglose[identificador].datos_opciones.donador_organos);
                jQuery('.piedras').val(pedido_desglose[identificador].disenopulsera.tipo_piedra);
            },
            error: function (err) {
                jQuery('.overlay_loader').hide();
                if(getUrlParameter('lang')=='en'){
                    mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                }else{
                    mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                }
            }
        })

            jQuery('.identificador').val(identificador);


    });
    jQuery('.contenido_modal').on('change','.talla_pedido',function(){

        var newtalla=jQuery(this).val();

        console.log(newtalla);

        if(newtalla) {
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'precioxtalla',
                    newtalla: newtalla,
                    current_moneda:localStorage.getItem('currency_code')
                },
                beforeSend: function () {
                    jQuery('.overlay_loader').show();
                },
                success: function (data) {
                    //var new_precio=JSON.parse(data);
                    jQuery('.overlay_loader').hide();
                    if (data.precio > 0) {
                        jQuery('.precio_talla').val(data.precio);
                    }
                },
                error: function (err) {
                    jQuery('.overlay_loader').hide();
                    if(getUrlParameter('lang')=='en'){
                        mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                    }else{
                        mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                    }
                }
            })
        }
    })

    jQuery('.btn_actualizar').click(function(){
        var identificador= jQuery('.identificador').val();
        var telefono_update=jQuery('.telefono_edit').val();
        var nacimiento_update=jQuery('.fecha_nacimiento_edit').val();
        var tipo_sangre_update=jQuery('.tipo_sangre_edit').val();
        var donador_organos_update=jQuery('.donador_organos').val();
        var new_precio_update=jQuery('.precio_talla').val();
        var piedras_update=jQuery('.piedras').val();
        var talla_s_update=jQuery('.talla_s').val();
        var talla_m_update=jQuery('.talla_m').val();
        var talla_l_update=jQuery('.talla_l').val();
        var talla_p_update=jQuery('.talla_p').val();
        var talla_pedido=jQuery('.talla_pedido').val();

        var pedido_desglose = JSON.parse(pedido_storage);
        pedido_desglose[identificador].datos_generales.telefono=telefono_update;
        pedido_desglose[identificador].datos_generales.fecha_nacimiento=nacimiento_update;
        pedido_desglose[identificador].datos_generales.fecha_nacimiento=nacimiento_update;
        pedido_desglose[identificador].datos_generales.tipo_sangre=tipo_sangre_update;
        pedido_desglose[identificador].datos_opciones.donador_organos=donador_organos_update;
        pedido_desglose[identificador].disenopulsera.precio_talla=new_precio_update;
        pedido_desglose[identificador].disenopulsera.tipo_piedra=piedras_update;

        switch(talla_pedido) {
            case 's':
                pedido_desglose[identificador].disenopulsera.tallas.talla_s='1';
                pedido_desglose[identificador].disenopulsera.tallas.talla_m='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_l='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_p='';
                break;
            case 'm':
                pedido_desglose[identificador].disenopulsera.tallas.talla_s='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_m='1';
                pedido_desglose[identificador].disenopulsera.tallas.talla_l='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_p='';
                break;

            case 'l':
                pedido_desglose[identificador].disenopulsera.tallas.talla_s='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_m='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_l='1';
                pedido_desglose[identificador].disenopulsera.tallas.talla_p='';
                break;

            case 'p':
                pedido_desglose[identificador].disenopulsera.tallas.talla_s='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_m='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_l='';
                pedido_desglose[identificador].disenopulsera.tallas.talla_p='1';
                break;

        }

        localStorage.setItem('eternita_pedido', JSON.stringify(pedido_desglose));
        jQuery('.edit_pedido').css('opacity',0);
        jQuery('.edit_pedido').css('visibility','hidden');
        if(getUrlParameter('lang')=='en'){
            mussalert('Success','It updated the order correctly','exito');
        }else{
            mussalert('Éxito','Se ha actualizado el pedido correctamente','exito');
        }


    });


    jQuery('.delete').click(function(){
        var identificador= jQuery(this).data('identificador');
        var selector= jQuery('.mensaje_confirm');
        selector.css('opacity','1');
        selector.css('visibility','visible');
        selector.find('p').html('Estas apunto de eliminar este pedido');
        jQuery('.overlay_multiusos').fadeIn();
        selector.find('.identificador').val(identificador);
    })

    jQuery('.btns div').on('click',function(){
        var pedido_desglose = JSON.parse(pedido_storage);
        var accion=jQuery(this).data('action');
        if(accion=='v'){
            var identificador= jQuery('.identificador').val();
            pedido_desglose.splice(identificador,1);
            localStorage.setItem('eternita_pedido', JSON.stringify(pedido_desglose));
            var selector= jQuery('.mensaje_confirm');
            selector.css('opacity','0');
            selector.css('visibility','hidden');
            if(getUrlParameter('lang')=='en'){
                mussalert('Success','It removed the order correctly','exito');
            }else{
                mussalert('Éxito','Se ha eliminado el pedido correctamente','exito');
            }
        }
        else{
            var selector= jQuery('.mensaje_confirm');
            selector.css('opacity','0');
            selector.css('visibility','hidden');
            jQuery('.overlay_multiusos').fadeOut();
        }
    })

    jQuery('.btns_pulsera div').on('click',function(){
        var accion=jQuery(this).data('action');
        if(accion=='v'){

            localStorage.removeItem('eternita_tallas');
            localStorage.removeItem('eternita_tallam');
            localStorage.removeItem('eternita_tallal');
            localStorage.removeItem('eternita_tallap');
            var selector= jQuery('.mensaje_confirm_pulsera');
            selector.css('opacity','0');
            selector.css('visibility','hidden');
            location.reload();
        }
        else{
            var selector= jQuery('.mensaje_confirm_pulsera');
            selector.css('opacity','0');
            selector.css('visibility','hidden');
            jQuery('.overlay_multiusos').fadeOut();
        }
    })

    jQuery('.btn_addpedido').click(function(){

        var pedido_desglose = JSON.parse(pedido_storage);
        var total=jQuery('.total_global_hidden').val();
        var nombre=jQuery('.nombre').val();
        var calle=jQuery('.calle').val();
        var num_exterior=jQuery('.num_exterior').val();
        var num_interior=jQuery('.num_interior').val();
        var cruzamientos=jQuery('.cruzamientos').val();
        var colonia=jQuery('.colonia').val();
        var ciudad=jQuery('.ciudad').val();
        var cp=jQuery('.cp').val();
        var telefono=jQuery('.telefono').val();
        var celular=jQuery('.celular').val();
        var email=jQuery('.email').val();
        var pais=jQuery('#country').val();
        var estado=jQuery('#state').val();
        var d = new Date();
        var erorres=[];
        var validaciones='';

        if(localStorage.getItem('datos_distribuidor')){
            var datos_distribuidor=localStorage.getItem('datos_distribuidor');
        }else{
            var datos_distribuidor='';
        }

        if(pedido_desglose ) {
            if(!jQuery.isEmptyObject(pedido_desglose)) {

                if(nombre!='' && calle !='' && num_exterior!='' && colonia!='' && ciudad!='' && cp!='' && celular!='' && pais!='' && estado!='') {
                    jQuery.ajax({
                        url: '/wp-admin/admin-ajax.php',
                        type: 'POST',
                        data: {
                            action: 'addpedido',
                            datos: pedido_desglose,
                            nombre: nombre,
                            calle: calle,
                            num_exterior: num_exterior,
                            num_interior: num_interior,
                            cruzamientos: cruzamientos,
                            colonia: colonia,
                            ciudad: ciudad,
                            cp: cp,
                            telefono: telefono,
                            celular: celular,
                            email: email,
                            pais: pais,
                            estado: estado,
                            folio: d.getTime(),
                            total: total,
                            datos_distribuidor: datos_distribuidor,
                            currency_code: localStorage.getItem('currency_code')

                        },
                        beforeSend: function () {
                            jQuery('.overlay_loader').show();
                        },
                        success: function (data) {
                            console.log(data);
                            jQuery('.overlay_loader').hide();
                            if (data > 0) {
                                localStorage.removeItem('eternita_pedido');
                                jQuery('.modal_formaspago').show();
                                jQuery('.overlay_multiusos').fadeIn();
                                jQuery('.id_pedido_res').val(data);
                                jQuery('.total_res').val(total);
                                jQuery('.currency_code').val(localStorage.getItem('currency_code'));

                            } else {
                                if(getUrlParameter('lang')=='en'){
                                    mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                                }else{
                                    mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                                }
                            }

                        },
                        error: function (err) {
                            jQuery('.overlay_loader').hide();
                            if(getUrlParameter('lang')=='en'){
                                mussalert('Error', 'An unknown error has occurred, try again. If the problem persists report the problem through our contact form', 'error');
                            }else{
                                mussalert('Error', 'Ha ocurrido un error desconocido, intente nuevamente. Si el problema persiste puede reportar el problema a través de nuestro formulario de contacto', 'error');
                            }
                        }
                    })
                }
                else{
                    jQuery('.overlay_loader').hide();
                    if(nombre==''){
                        erorres.push('Nombre');
                    }
                    if(calle==''){
                        erorres.push('Calle');
                    }
                    if(num_exterior==''){
                        erorres.push('N�mero exterior');
                    }
                    if(cruzamientos==''){
                        erorres.push('Cruzamientos');
                    }
                    if(colonia==''){
                        erorres.push('Colonia');
                    }
                    if(ciudad==''){
                        erorres.push('Ciudad');
                    }
                    if(cp==''){
                        erorres.push('C.P');
                    }
                    if(celular==''){
                        erorres.push('Celular');
                    }
                    if(pais==''){
                        erorres.push('Pais');
                    }
                    if(estado==''){
                        erorres.push('Estado');
                    }
                    for (var i = 0; i < erorres.length; i++)
                    {
                        validaciones += erorres[i]+'<br>';
                    }

                    if(getUrlParameter('lang')=='en'){
                        var mensaje='Missing the following information required:<br>' +
                            ''+validaciones+''
                    }else{
                        var mensaje='Falta la siguiente información requerida:<br>' +
                            ''+validaciones+''
                    }

                    mussalert('Error',mensaje,'error');

                }
            }
        }
        else{

            if(getUrlParameter('lang')=='en'){
                mussalert('Error','He has not made any request','error');
            }else{
                mussalert('Error','No se ha realizado ningun pedido','error');
            }
        }
    })


    jQuery('.btn_pago').click(function(){
        jQuery('.gracias').show();
    })

});

function mussalert(title,contenido,type){
    var selector= jQuery('.mensaje');
    selector.css('opacity','1');
    selector.css('visibility','visible')
    selector.find('h2').text(title);
    selector.find('p').html(contenido);
    jQuery('.overlay_multiusos').fadeIn();
    if(selector.hasClass('exito')){
        selector.removeClass('exito');
    }
    if(selector.hasClass('error')){
        selector.removeClass('error');
    }
    if(selector.hasClass('advertencia')){
        selector.removeClass('advertencia');
    }
    selector.addClass(type);
}

function mussalertpedido(title,contenido,type){
    var selector= jQuery('.mensaje_pedido');
    selector.css('opacity','1');
    selector.css('visibility','visible')
    selector.find('h2').text(title);
    selector.find('p').html(contenido);
    jQuery('.overlay_multiusos').fadeIn();
    if(selector.hasClass('exito')){
        selector.removeClass('exito');
    }
    if(selector.hasClass('error')){
        selector.removeClass('error');
    }
    if(selector.hasClass('advertencia')){
        selector.removeClass('advertencia');
    }
    selector.addClass(type);
}

/*function mussalert_confirm(title,e){
    try{
        var selector= jQuery('.mensaje_confirm');
        selector.css('opacity','1');
        selector.css('visibility','visible');
        selector.find('p').html(title);
        jQuery('.overlay_multiusos').fadeIn();
    }
    catch(e) {
        console.log('hola');
    }

}*/

function insert_pedido(){
    var nombre=jQuery('.nombre').val();
    var telefono=jQuery('.telefono').val();
    var fecha_nacimiento=jQuery('.fecha_nacimiento').val();
    var tipo_sangre=jQuery('.tipo_sangre').val();
    var dato_medico=jQuery('.datos_medico').val();
    var tipo_letra=jQuery('.tipo_letra').val();
    var donador= jQuery('.donador').val();
    var piedra= jQuery('.piedra').val();
    var precio_talla= jQuery('.precio_talla').val();
    var erorres=[];
    var validaciones='';
    var pedidos=[];
    var pedido_desglose = JSON.parse(localStorage.getItem('eternita_pedido'));
    var img='';

    console.log(precio_talla);

    if(nombre,telefono,fecha_nacimiento,tipo_sangre) {
        var atributos_pedido = {};
        var tallas_pedido=localStorage.getItem('eternita_tallas');
        var tallam_pedido=localStorage.getItem('eternita_tallam');
        var tallal_pedido=localStorage.getItem('eternita_tallal');
        var tallap_pedido=localStorage.getItem('eternita_tallap');

        if(tallas_pedido){
            var cant_tallaspedido=tallas_pedido;
            var talla_sels='s'
        }
        else{
            var cant_tallaspedido=0;
            var talla_sels=''
        }

        if(tallam_pedido){
            var cant_tallampedido=tallam_pedido;
            var talla_selm='m'
        }
        else{
            var cant_tallampedido=0;
            var talla_selm=''
        }

        if(tallal_pedido){
            var cant_tallalpedido=tallal_pedido;
            var talla_sell='l'
        }
        else{
            var cant_tallalpedido=0;
            var talla_sell=''
        }

        if(tallap_pedido){
            var cant_tallappedido=tallap_pedido;
            var talla_selp='p'
        }
        else{
            var cant_tallappedido=0;
            var talla_selp=''
        }

        atributos_pedido.disenopulsera = {
            'imgpulsera': localStorage.getItem('img_pulsera'),
            'tipo_letra': tipo_letra,
            'tipo_piedra': piedra,
            'precio_talla': precio_talla,
            'moneda_precio': localStorage.getItem('currency_code'),
            'tallas': {'talla_s': cant_tallaspedido, 'talla_m': cant_tallampedido, 'talla_l': cant_tallalpedido, 'talla_p': cant_tallappedido}

        };
        atributos_pedido.datos_generales = {
            'nombre': nombre,
            'telefono': telefono,
            'fecha_nacimiento': fecha_nacimiento,
            'tipo_sangre': tipo_sangre
        };
        atributos_pedido.datos_opciones = {
            'donador_organos': donador,
            'datos_medicos': dato_medico
        };
        if(pedido_desglose) {
            for(var m=0; m < pedido_desglose.length; m++){
                //console.log(pedido_desglose[m]);
                pedidos.push(pedido_desglose[m]);
            }
            pedidos.push(atributos_pedido);
        }
        else{
            pedidos.push(atributos_pedido);
        }


        localStorage.setItem('eternita_pedido', JSON.stringify(pedidos));

        var contador_carrito = JSON.parse(localStorage.getItem('eternita_pedido'));
        jQuery('.contador_pedidos').text(contador_carrito.length);
        var list='';
        list +='<li><p>Nombre: '+nombre+'</p><p>Talla: '+talla_sels+talla_selm+talla_sell+talla_selp+'</p><p>Precio: '+parseInt(precio_talla).formatMoney()+'</p></li>';
        jQuery('.list_pedidos').find('ul').append(list);
        jQuery('.contenedor_datos').find('input').val('');
        jQuery('.contenedor_datos').find('select').val('');
        jQuery('.contenedor_datos').find('textarea').val('');
        jQuery('.nombre_pulseradiseno').text('Tu nombre aqui');
        list='';

        localStorage.removeItem('eternita_tallas');
        localStorage.removeItem('eternita_tallam');
        localStorage.removeItem('eternita_tallal');
        localStorage.removeItem('eternita_tallap');
        localStorage.removeItem('img_pulsera');
        jQuery('.overlay_loader').hide();
        jQuery('.overlay_multiusos2').fadeOut();
        if(getUrlParameter('lang')=='en'){
            mussalertpedido('Success','It added the order correctly','exito');
        }else{
            mussalertpedido('Éxito','Se ha agregado el pedido correctamente','exito');
        }
    }
    else{
        jQuery('.overlay_loader').hide();

        if(nombre==''){
            erorres.push('Nombre');
        }
        if(telefono==''){
            erorres.push('Telefono');
        }
        if(fecha_nacimiento==''){
            erorres.push('Fecha nacimiento');
        }
        if(tipo_sangre==''){
            erorres.push('Tipo de sangre');
        }
        for (var i = 0; i < erorres.length; i++)
        {
            validaciones += erorres[i]+'<br>';
        }

        if(getUrlParameter('lang')=='en'){
            var mensaje='Missing the following information required:<br>' +
                ''+validaciones+''
        }else{
            var mensaje='Falta la siguiente información requerida:<br>' +
                ''+validaciones+''
        }

        mussalert('Error',mensaje,'error');
    }
}

function mussalert_close(){
    var selector= jQuery('.mensaje');
    selector.css('opacity','0');
    selector.css('visibility','hidden');
    jQuery('.overlay_multiusos').fadeOut();
}

function mussalert_close_pedido(){
    var selector= jQuery('.mensaje');
    selector.css('opacity','0');
    selector.css('visibility','hidden');
    jQuery('.overlay_multiusos').fadeOut();
}

function convertCanvasToImage(canvas){
    var image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image.src;
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};
