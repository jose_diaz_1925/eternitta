<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eternita');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<N?uL.OR=/)R;jI<7eyzUA99X*6Sa).%9VCCgQ<NLP,.Y_,1I*;),;9z>KTuh7)$');
define('SECURE_AUTH_KEY',  '=uNh~QRA<Ny{r)/-7c_$u^]UxDX!kJ*b!#7Wr]RS+_r91@=j3mUh;C%)VzV7*`R.');
define('LOGGED_IN_KEY',    '9-fi:Wq1wxK_sVt#*h$WzObG~-wjdZ1M[#10[Xz`QBJ#K5&.x`uUVl?knKodsX1G');
define('NONCE_KEY',        'JZ%GpVD(a14D]C?jYGN2I)b>7/.HK~7LU=cJ4~E|&}g2@qwy.tihA0T?0cq.(|%$');
define('AUTH_SALT',        '$qr1^ix]KAhB#Vi!TBm6+_ruVWYej=~/+{oL[ fbACx-TaKGIy`76!k*F3#mg{g$');
define('SECURE_AUTH_SALT', '2f uzZ-3#l.9_$${U-x(~h+j%5K&M ses4ps&gJ( cct*`(;)K!3xpx?0y&~1V`^');
define('LOGGED_IN_SALT',   'j|A9yeT:$*yRCoXJ2ML;0NP$^`4_Q+zm{iMOnQ<:J|Wt7`CupUDl#rh9x|*E]`4R');
define('NONCE_SALT',       '!6E6v_Wh6f:F~xs,w,T3DU`cTJJA&iq&+MZl&nN&RtdB_<bKD+c_td j+_>:n)Jd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
